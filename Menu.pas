﻿unit Menu;

interface

uses Windows, apiActions, apiMenu;

type
  TAIMPMenuService = class(TObject)
  private
    function GetBuiltInMenu(ID: Integer): IAIMPMenuItem;
  public
    constructor Create;
  end;

  TAIMPMenuItemEventHandler = class(TInterfacedObject, IAIMPActionEvent)
  public
    procedure OnExecute(Data: IInterface); stdcall;
  end;

  TAIMPMenuItemOnShow = class(TInterfacedObject, IAIMPActionEvent)
  public
    procedure OnExecute(Data: IInterface); stdcall;
  end;

implementation

uses
  ActiveX, defines, apiObjects, apiPlaylists, System.Generics.Collections,
  System.Sysutils, apiFileManager, apiWrappers {$IFDEF DEBUG}, InfoForm, apiGui
  {$ENDIF};

function TAIMPMenuService.GetBuiltInMenu(ID: Integer): IAIMPMenuItem;
var
  AMenuService: IAIMPServiceMenuManager;
begin
  if CoreGetService(IAIMPServiceMenuManager, AMenuService) then
    try
      CheckResult(AMenuService.GetBuiltIn(ID, Result));
    finally
      AMenuService := nil;
    end;
end;

constructor TAIMPMenuService.Create;
var
  AMenuItem: IAIMPMenuItem;
begin
  if Succeeded(CoreIntf.CreateObject(IID_IAIMPMenuItem, AMenuItem)) then
  begin
    with AMenuItem do
    begin
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_ID, MakeString('awa.skiptrack.enable')));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_NAME, MakeString('SkipTrack')));
      CheckResult(SetValueAsInt32(AIMP_MENUITEM_PROPID_CHECKED, FEnable.ToInteger));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_EVENT, TAIMPMenuItemEventHandler.Create));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_PARENT,
        GetBuiltInMenu(AIMP_MENUID_PLAYER_PLAYLIST_MISCELLANEOUS)));
      CheckResult(SetValueAsInt32(AIMP_MENUITEM_PROPID_STYLE, AIMP_MENUITEM_STYLE_CHECKBOX));
    end;
    CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServiceMenuManager, AMenuItem));
  end;

  if Succeeded(CoreIntf.CreateObject(IID_IAIMPMenuItem, AMenuItem)) then
  begin
    with AMenuItem do
    begin
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_ID, MakeString('awa.skiptrack.addexcept')));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_NAME, LangLoadStringEx('skiptrack_menu\AddExcept')));

      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_EVENT, TAIMPMenuItemEventHandler.Create));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_EVENT_ONSHOW, TAIMPMenuItemOnShow.Create));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_PARENT,
        GetBuiltInMenu(AIMP_MENUID_PLAYER_PLAYLIST_CONTEXT_FUNCTIONS)));
      CheckResult(SetValueAsInt32(AIMP_MENUITEM_PROPID_STYLE, AIMP_MENUITEM_STYLE_NORMAL));
    end;
    CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServiceMenuManager, AMenuItem));
  end;

  {$IFDEF DEBUG}
  if Succeeded(CoreIntf.CreateObject(IID_IAIMPMenuItem, AMenuItem)) then
  begin
    with AMenuItem do
    begin
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_ID, MakeString('awa.skiptrack.debug')));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_NAME, MakeString('SkipTrack Info')));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_EVENT, TAIMPMenuItemEventHandler.Create));
      CheckResult(SetValueAsObject(AIMP_MENUITEM_PROPID_PARENT,
        GetBuiltInMenu(AIMP_MENUID_PLAYER_PLAYLIST_MISCELLANEOUS)));
      CheckResult(SetValueAsInt32(AIMP_MENUITEM_PROPID_STYLE, AIMP_MENUITEM_STYLE_NORMAL));
    end;
    CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServiceMenuManager, AMenuItem));
  end;
  {$ENDIF}
end;

procedure TAIMPMenuItemOnShow.OnExecute(Data: IInterface);
var
  AMenuItem: IAIMPMenuItem;
  APlaylistManager: IAIMPServicePlaylistManager;
  APlaylist: IAIMPPlaylist;
  AFilesList: IAIMPObjectList;
begin
  AMenuItem := Data as IAIMPMenuItem;

  if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
  begin
    try
      if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
        if Succeeded(CoreIntf.CreateObject(IID_IAIMPObjectList, AFilesList)) then
          if Succeeded(APlaylist.GetFiles(AIMP_PLAYLIST_GETFILES_FLAGS_SELECTED_ONLY +
            AIMP_PLAYLIST_GETFILES_FLAGS_COLLAPSE_VIRTUAL, AFilesList)) then
          begin
            if AFilesList.GetCount <= 0 then
              AMenuItem.SetValueAsInt32(AIMP_MENUITEM_PROPID_ENABLED, 0)
            else
              AMenuItem.SetValueAsInt32(AIMP_MENUITEM_PROPID_ENABLED, 1)
          end;
    finally
      APlaylistManager := nil;
      APlaylist := nil;
      AFilesList := nil;
    end;
  end;
end;

procedure TAIMPMenuItemEventHandler.OnExecute(Data: IInterface);
var
  AMenuItem: IAIMPMenuItem;
  AString: IAIMPString;
  AInt: Integer;
  APlaylistManager: IAIMPServicePlaylistManager;
  APlaylist: IAIMPPlaylist;
  APlaylistItem: IAIMPPlaylistItem;
  SongList: TList<IAIMPPlaylistItem>;
  AFileInfo: IAIMPFileInfo;
  {$IFDEF DEBUG}AUIService: IAIMPServiceUI; {$ENDIF}
begin
  try
    AMenuItem := Data as IAIMPMenuItem;

    if Succeeded(AMenuItem.GetValueAsObject(AIMP_MENUITEM_PROPID_ID, IID_IAIMPString, AString)) then
    begin
      if IAIMPStringToString(AString) = 'awa.skiptrack.enable' then
      begin
        if PropListGetInt32A(AMenuItem, AIMP_MENUITEM_PROPID_CHECKED) <> 0 then
          FEnable := true
        else
          FEnable := False;

        Exit;
      end;

      if IAIMPStringToString(AString) = 'awa.skiptrack.addexcept' then
      begin
        if Succeeded(CoreIntf.QueryInterface(IID_IAIMPServicePlaylistManager, APlaylistManager)) then
        begin
          try
            if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
            begin
              SongList := TList<IAIMPPlaylistItem>.Create;

              for AInt := 0 to APlaylist.GetItemCount - 1 do
                if Succeeded(APlaylist.GetItem(AInt, IID_IAIMPPlaylistItem, APlaylistItem)) then
                  SongList.Add(APlaylistItem);

              if SongList.Count <= 0 then
                Exit;

              for AInt := 0 to SongList.Count - 1 do
              begin
                if PropListGetInt32A(SongList[AInt], AIMP_PLAYLISTITEM_PROPID_SELECTED) <> 0 then
                  if Succeeded(SongList[AInt].GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_FILEINFO, IID_IAIMPFileInfo,
                    AFileInfo)) then
                    if PropListGetStr(AFileInfo, AIMP_FILEINFO_PROPID_ARTIST, AString) then
                      if FExceptions.IndexOf(IAIMPStringToString(AString)) = -1 then
                      begin
                        FExceptions.Add(IAIMPStringToString(AString));
                        if Assigned(FOptionFrame.OptionFrame) then
                        begin
                          FOptionFrame.OptionFrame.AddArtist(IAIMPStringToString(AString));
                          FOptionFrame.OptionFrame.OnModified(nil);
                        end;
                        {$IFDEF DEBUG}
                        if Assigned(FInfoForm.Form) then
                          FInfoForm.DebugMessageS(IAIMPStringToString(AString), 'Исполнитель добавлен в исключения',
                            STYLE_NOTICE);
                        {$ENDIF}
                      end;
              end;
              FExceptions.SaveToFile(CoreGetProfilePath + '\exceptions.txt', TEncoding.UTF8);
              Exit;
            end;
          finally
            APlaylistManager := nil;
            APlaylist := nil;
            APlaylistItem := nil;
            AFileInfo := nil;
            FreeAndNil(SongList);
          end;
        end;
      end;

      {$IFDEF DEBUG}
      if IAIMPStringToString(AString) = 'awa.skiptrack.debug' then
        if not Assigned(FInfoForm.Form) then
          if CoreGetService(IID_IAIMPServiceUI, AUIService) then
          begin
            FInfoForm := TInfoForm.Create(AUIService);
            FInfoForm.Form.SetValueAsInt32(AIMPUI_CONTROL_PROPID_VISIBLE, 1);
          end;
      {$ENDIF}
    end;
  finally
    AMenuItem := nil;
    AString := nil;
  end;
end;

end.
