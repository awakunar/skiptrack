﻿unit PlaybackExtension;

interface

uses Windows, apiPlayer, apiPlaylists;

type
  TAIMPExtPlaybackQueue = class(TInterfacedObject, IAIMPExtensionPlaybackQueue)
  private
    function GetNextTrack(const Playlist: IAIMPPlaylist): IAIMPPlaylistItem;
  protected
    { IAIMPExtensionPlaybackQueue }
    function GetNext(Current: IUnknown; Flags: DWORD; QueueItem: IAIMPPlaybackQueueItem): LongBool; stdcall;
    function GetPrev(Current: IUnknown; Flags: DWORD; QueueItem: IAIMPPlaybackQueueItem): LongBool; stdcall;
    procedure OnSelect(Item: IAIMPPlaylistItem; QueueItem: IAIMPPlaybackQueueItem); stdcall;
  end;

implementation

uses {$IFDEF DEBUG}InfoForm, {$ENDIF} Defines, CheckSong, System.Sysutils,
  apiWrappers;

function TAIMPExtPlaybackQueue.GetNextTrack(const Playlist: IAIMPPlaylist): IAIMPPlaylistItem;
var
  AError: Integer;
begin
  if Playlist.GetItemCount < 1 then
    Exit(nil);

  AError := 0;
  while true do
  begin
    if Succeeded(Playlist.GetItem(Random(Playlist.GetItemCount), IID_IAIMPPlaylistItem, Result)) then
    begin
      // Проверка на бесконечный цикл
      Inc(AError);
      if AError > (Playlist.GetItemCount * 2) then
      begin
        if FPlaybackQueue[Playlist].SongItem.Count < Playlist.GetItemCount then
        begin
          if Not FPlaybackQueue[Playlist].SongItem.Contains(Result) then
          begin
            {$IFDEF DEBUG}
            if Assigned(FInfoForm.Form) then
              FInfoForm.DebugMessageG(Result, AError.ToString + ' попыток. Песен подходящим по условиям не найдено',
                STYLE_WARNING);
            {$ENDIF}
            if FPlaybackQueue[Playlist].LastArtist.Count >= 1 then
              FPlaybackQueue[Playlist].LastArtist.Dequeue;

            { if FPlaybackQueue[Playlist].OverrideSettings then
              begin
              if FPlaybackQueue[Playlist].PlstFilter in [2, 3, 6, 7] then
              begin
              Inc(FPlaybackQueue[Playlist].PlsPlaycount);
              FPlaybackQueue[Playlist].NoMathSongList.Clear;
              end;
              end
              else if FPlaybackQueue.Filter in [2, 3, 6, 7] then
              begin
              Inc(FPlaybackQueue.Playcount);
              FPlaybackQueue[Playlist].NoMathSongList.Clear;
              end; }

            Exit;
          end;
        end
        else
        begin
          {$IFDEF DEBUG}
          if Assigned(FInfoForm.Form) then
            FInfoForm.DebugMessageS(GetPlaylistName(Playlist), 'Плейлист закончен - нечего добавлять', STYLE_NOTICE);
          {$ENDIF}
          FPlaybackQueue[Playlist].PlaylistEnd := true;
          Exit(nil);
        end;
      end;

      // Если трека нет в истории и списке неподходящих песен
      if (not FPlaybackQueue[Playlist].SongItem.Contains(Result)) and
        (not FPlaybackQueue[Playlist].NoMathSongList.Contains(Result)) then
        if IsValid(Playlist, Result) then
          Exit;
    end;
  end;
end;

function TAIMPExtPlaybackQueue.GetNext(Current: IUnknown; Flags: DWORD; QueueItem: IAIMPPlaybackQueueItem): LongBool;
var
  APlaylist: IAIMPPlaylist;
  APlaylistItem, ANextItem: IAIMPPlaylistItem;
begin
  // Плагин выключен
  if not FEnable then
    Exit(False);

  if FShuffle then
  begin
    case Flags of
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_BEGINNING:
        begin
          try
            APlaylist := Current as IAIMPPlaylist;
            if Succeeded(APlaylist.GetItem(0, IID_IAIMPPlaylistItem, APlaylistItem)) then
              CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, APlaylistItem));
          finally
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_CURSOR:
        begin
          try
            APlaylist := Current as IAIMPPlaylist;
            FPlaybackQueue.CreatePlaylist(APlaylist);

            with FPlaybackQueue[APlaylist] do
            begin
              if SongItem.Count > -1 then
              begin
                while PlaybackCursor + 2 > SongItem.Count - 1 do
                  SongItem.Add(GetNextTrack(APlaylist));

                APlaylistItem := SongItem.Items[PlaybackCursor + 1];
              end
              else
              begin
                SongItem.Add(GetNextTrack(APlaylist));
                APlaylistItem := SongItem.Last;
              end;
            end;

            CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, APlaylistItem));
          finally
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_ITEM:
        begin
          try
            APlaylistItem := Current as IAIMPPlaylistItem;
            if Succeeded(APlaylistItem.GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_PLAYLIST, IID_IAIMPPlaylist, APlaylist))
            then
            begin
              FPlaybackQueue.CreatePlaylist(APlaylist);

              with FPlaybackQueue[APlaylist] do
              begin
                if SongItem.Count > -1 then
                begin
                  while PlaybackCursor + 2 > SongItem.Count - 1 do
                    SongItem.Add(GetNextTrack(APlaylist));

                  if SongItem.Count - (SongItem.IndexOf(APlaylistItem) + 1) >= 1 then
                    ANextItem := SongItem[SongItem.IndexOf(APlaylistItem) + 1]
                  else
                    ANextItem := nil;
                end
                else
                  ANextItem := nil;
              end;

              CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, ANextItem));
            end;
          finally
            APlaylist := nil;
            APlaylistItem := nil;
            ANextItem := nil;
          end;
        end;
    end;
    Result := true;
  end
  else
    Result := False;
end;

function TAIMPExtPlaybackQueue.GetPrev(Current: IUnknown; Flags: DWORD; QueueItem: IAIMPPlaybackQueueItem): LongBool;
var
  APlaylist: IAIMPPlaylist;
  APlaylistItem: IAIMPPlaylistItem;
begin
  // Плагин выключен
  if not FEnable then
    Exit(False);

  if FShuffle then
  begin
    case Flags of
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_BEGINNING:
        begin
          try
            APlaylist := Current as IAIMPPlaylist;
            if Succeeded(APlaylist.GetItem(APlaylist.GetItemCount, IID_IAIMPPlaylistItem, APlaylistItem)) then
              CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, APlaylistItem));
          finally
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_CURSOR:
        begin
          try
            APlaylist := Current as IAIMPPlaylist;
            FPlaybackQueue.CreatePlaylist(APlaylist);

            with FPlaybackQueue[APlaylist] do
            begin
              if (SongItem.Count > -1) and (PlaybackCursor > 0) then
              begin
                try
                  APlaylistItem := SongItem[PlaybackCursor - 1]
                except
                  APlaylistItem := nil;
                end;
              end
              else
                APlaylistItem := nil;
            end;

            CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, APlaylistItem));
          finally
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
      AIMP_PLAYBACKQUEUE_FLAGS_START_FROM_ITEM:
        begin
          try
            APlaylistItem := Current as IAIMPPlaylistItem;
            if Succeeded(APlaylistItem.GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_PLAYLIST, IID_IAIMPPlaylist, APlaylist))
            then
            begin
              FPlaybackQueue.CreatePlaylist(APlaylist);

              with FPlaybackQueue[APlaylist] do
              begin
                if SongItem.Count > -1 then
                begin
                  if SongItem.Count - (SongItem.IndexOf(APlaylistItem) - 1) >= 1 then
                    APlaylistItem := SongItem[SongItem.IndexOf(APlaylistItem) - 1]
                  else
                    APlaylistItem := nil;
                end
                else
                  APlaylistItem := nil;
              end;

              CheckResult(QueueItem.SetValueAsObject(AIMP_PLAYBACKQUEUEITEM_PROPID_PLAYLISTITEM, APlaylistItem));
            end;
          finally
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
    end;
    Result := true;
  end
  else
    Result := False;
end;

procedure TAIMPExtPlaybackQueue.OnSelect(Item: IAIMPPlaylistItem; QueueItem: IAIMPPlaybackQueueItem);
var
  APlaylist: IAIMPPlaylist;
begin
  if FEnable and FShuffle then
  begin
    if Succeeded(Item.GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_PLAYLIST, IID_IAIMPPlaylist, APlaylist)) then
    begin
      try
        FPlaybackQueue.CreatePlaylist(APlaylist);
        with FPlaybackQueue[APlaylist] do
        begin
          if SongItem.Contains(Item) then
          begin
            if abs(PlaybackCursor - SongItem.IndexOf(Item)) < 3 then
              PlaybackCursor := SongItem.IndexOf(Item)
          end
          else
          begin

            if (PlaybackCursor = -1) and (SongItem.Count <= 3) then
            begin
              SongItem.Insert(0, Item);
              PlaybackCursor := 0;

              {$IFDEF DEBUG}
              if Assigned(FInfoForm.Form) then
                FInfoForm.DebugMessageO(Item, 'Добавлен в начало списка');
              {$ENDIF}
            end
            else
            begin
              SongItem.Insert(PlaybackCursor + 1, Item);
              Inc(PlaybackCursor);

              {$IFDEF DEBUG}
              if Assigned(FInfoForm.Form) then
                FInfoForm.DebugMessageO(Item, 'Добавлен в позицию ' + IntToStr(PlaybackCursor + 1));
              {$ENDIF}
            end;
          end;
        end;

        if FCurrentPlaylist <> APlaylist then
        begin
          if Assigned(FCurrentPlaylist) and FPlaybackQueue.ContainsKey(FCurrentPlaylist) then
          begin
            if FPlaybackQueue[FCurrentPlaylist].PlaylistEnd then
            begin
              {$IFDEF DEBUG}
              if Assigned(FInfoForm.Form) then
                FInfoForm.DebugMessageS(GetPlaylistName(FCurrentPlaylist), 'Плейлист закончен, очищаем очередь',
                  STYLE_NOTICE);
              {$ENDIF}
              FPlaybackQueue.ClearPlaylist(FCurrentPlaylist);
            end;
          end;
          FCurrentPlaylist := APlaylist;
        end;
      finally
        APlaylist := nil;
      end;
    end;
  end;
  {$IFDEF DEBUG}
  if FCurrentSong <> Item then
  begin
    if Assigned(FInfoForm.Form) then
      FInfoForm.ShowPlaybackQueue;
    FCurrentSong := Item;
  end;
  {$ENDIF}
end;

end.
