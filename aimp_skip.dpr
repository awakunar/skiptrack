﻿library aimp_skip;

uses
  apiPlugin,
  Main in 'Main.pas',
  {$IFDEF DEBUG} InfoForm in 'InfoForm.pas', {$ENDIF }
  Defines in 'Defines.pas',
  Menu in 'Menu.pas',
  PlaylistManager in 'PlaylistManager.pas',
  OptionsForm in 'OptionsForm.pas',
  OptionsFrame in 'OptionsFrame.pas',
  PlaybackQueue in 'PlaybackQueue.pas',
  CheckSong in 'CheckSong.pas',
  PlaybackExtension in 'PlaybackExtension.pas';

{$R *.res}

function AIMPPluginGetHeader(out Header: IAIMPPlugin): HRESULT; stdcall;
begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF }
  try
    Header := TAIMPTrackSkip.Create;
    Result := S_OK;
  except
    Result := E_UNEXPECTED;
  end;
end;

exports
  AIMPPluginGetHeader;

begin

end.
