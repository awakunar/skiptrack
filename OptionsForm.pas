﻿unit OptionsForm;

interface

uses
  Windows, Classes, apiGUI, apiWrappersGUI, apiObjects, apiPlaylists,
  System.Generics.Collections;

const
  NullRect: TRect = (Left: 0; Top: 0; Right: 0; Bottom: 0);
  Enable: Integer = 1;
  Disable: Integer = 0;

type
  TPlaylist = class(TObject)
    OverrideSettings: Boolean;
    Playlist: IAIMPPlaylist;
    PlstFilter: Integer;
    PlsArtists: Integer;
    PlsDays: Integer;
    PlsPlaycount: Integer;
    PlsExcept: Boolean;
    PlsQueueSave: Boolean;
    PlsExceptFav: Boolean;
  public
    destructor Destroy; override;
  end;

  TPlaylistSettings = class(TObjectDictionary<Integer, TPlaylist>)
  private
    FPlaylist: TPlaylist;
  public
    procedure AddPlaylist(const Index: Integer; const Playlist: IAIMPPlaylist);
  end;

  TOptionsFrame = class(TInterfacedObject, IAIMPUIChangeEvents, IAIMPUIFormEvents, IAIMPUITreeListEvents,
    IAIMPUIKeyboardEvents)
  private
    FPlaylistSettings: TPlaylistSettings;
    FForm: IAIMPUIForm;
    FOnModified: TNotifyEvent;
    FModified: Boolean;
    FMainLoc, FPlsLoc: IAIMPString;
    // Playlists
    cbbPlaylists: IAIMPUIComboBox;
    gbPlaylists: IAIMPUIGroupBox;
    cbDays: IAIMPUICheckBox;
    cbPlaycount: IAIMPUICheckBox;
    cbArtists: IAIMPUICheckBox;
    seDays: IAIMPUISpinEdit;
    sePlaycount: IAIMPUISpinEdit;
    seArtists: IAIMPUISpinEdit;
    cbQueueSave: IAIMPUICheckBox;
    // Exceptions
    gbExcept: IAIMPUIGroupBox;
    cbFav: IAIMPUICheckBox;
    edArtist: IAIMPUIEdit;
    tlExceptions: IAIMPUITreeList;
    btnAdd: IAIMPUIButton;
    btnDelete: IAIMPUIButton;
    procedure OnChanged(Sender: IInterface); stdcall;
    procedure DoModified;
    procedure HandlerComboBox(const Sender: IUnknown);
    procedure SelectFirstNode;
    procedure SpinEditState;
    procedure ExceptState(const Index: Integer);
    procedure SetCheckBoxState(Name: IAIMPUICheckBox; const State: Integer);
    procedure SetFilter(const Filter: Integer);
    function GetFilter: Integer;
    // IAIMPUIFormEvents
    procedure OnActivated(Sender: IAIMPUIForm); overload; stdcall;
    procedure OnCloseQuery(Sender: IAIMPUIForm; var CanClose: LongBool); stdcall;
    procedure OnCreated(Sender: IAIMPUIForm); stdcall;
    procedure OnDeactivated(Sender: IAIMPUIForm); stdcall;
    procedure OnDestroyed(Sender: IAIMPUIForm); stdcall;
    procedure OnLocalize(Sender: IAIMPUIForm); stdcall;
    procedure OnShortCut(Sender: IAIMPUIForm; Key: Word; Modifiers: Word; var Handled: LongBool); stdcall;
    // IAIMPUITreeListEvents
    procedure OnColumnClick(Sender: IAIMPUITreeList; ColumnIndex: Integer); stdcall;
    procedure OnFocusedColumnChanged(Sender: IAIMPUITreeList); stdcall;
    procedure OnFocusedNodeChanged(Sender: IAIMPUITreeList); stdcall;
    procedure OnNodeChecked(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode); stdcall;
    procedure OnNodeDblClicked(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode); stdcall;
    procedure OnSelectionChanged(Sender: IAIMPUITreeList); stdcall;
    procedure OnSorted(Sender: IAIMPUITreeList); stdcall;
    procedure OnStructChanged(Sender: IAIMPUITreeList); stdcall;
    // IAIMPUIKeyboardEvents
    procedure OnEnter(Sender: IUnknown); stdcall;
    procedure OnExit(Sender: IUnknown); stdcall;
    procedure OnKeyDown(Sender: IUnknown; var Key: Word; Modifiers: Word); stdcall;
    procedure OnKeyPress(Sender: IUnknown; var Key: WideChar); stdcall;
    procedure OnKeyUp(Sender: IUnknown; var Key: Word; Modifiers: Word); stdcall;
  protected
    FService: IAIMPServiceUI;
  protected
    procedure CreateExceptions(AParent: IAIMPUIWinControl);
    procedure CreatePlaylist(AParent: IAIMPUIWinControl);
    procedure CreateLabel(Parent: IAIMPUIWinControl; const Name: IAIMPString; const Bounds: TRect);
    function CreateCheckBox(Parent: IAIMPUIWinControl; const Name: IAIMPString; EventsHandler: IUnknown;
      const Bounds: TRect): IAIMPUICheckBox;
    function CreateSpinEdit(Parent: IAIMPUIWinControl; const Name: IAIMPString; EventsHandler: IUnknown;
      const Bounds: TRect; MaskKey: string): IAIMPUISpinEdit;
    function CreateTreeList(Parent: IAIMPUIWinControl; const Name: IAIMPString; Placement: TAIMPUIControlPlacement)
      : IAIMPUITreeList;
  public
    property Form: IAIMPUIForm read FForm write FForm;
    constructor Create(Parent: HWND; AService: IAIMPServiceUI);
    procedure ApplyLocalization;
    procedure ConfigLoad;
    procedure ConfigSave;
    property OnModified: TNotifyEvent read FOnModified write FOnModified;
    procedure AddArtist(const Artist: string);
  end;

implementation

uses
  defines, apiWrappers, SysUtils, apiMenu, IOUtils;

destructor TPlaylist.Destroy;
begin
  Playlist := nil;
  inherited;
end;

procedure TPlaylistSettings.AddPlaylist(const Index: Integer; const Playlist: IAIMPPlaylist);
begin
  if not self.ContainsKey(Index) then
  begin
    FPlaylist := TPlaylist.Create;
    with FPlaybackQueue[Playlist] do
    begin
      FPlaylist.Playlist := Playlist;
      FPlaylist.OverrideSettings := OverrideSettings;
      FPlaylist.PlstFilter := PlstFilter;
      FPlaylist.PlsArtists := PlsArtists;
      FPlaylist.PlsDays := PlsDays;
      FPlaylist.PlsPlaycount := PlsPlaycount;
      FPlaylist.PlsExcept := PlsExcept;
      FPlaylist.PlsQueueSave := PlsQueueSave;
    end;
    self.Add(Index, FPlaylist);
  end;
end;

procedure TOptionsFrame.ApplyLocalization;
var
  AMenuService: IAIMPServiceMenuManager;
  AMenuItem: IAIMPMenuItem;
begin
  // Локализация пунктов меню
  if CoreGetService(IAIMPServiceMenuManager, AMenuService) then
  begin
    try
      if Succeeded(AMenuService.GetByID(MakeString('awa.skiptrack.addexcept'), AMenuItem)) then
        CheckResult(AMenuItem.SetValueAsObject(AIMP_MENUITEM_PROPID_NAME,
          LangLoadStringEx('skiptrack_menu\AddExcept')));
    finally
      AMenuService := nil;
      AMenuItem := nil;
    end;
  end;

  CheckResult(seDays.SetValueAsObject(AIMPUI_SPINEDIT_PROPID_DISPLAYMASK,
    LangLoadStringEx('skiptrack_options\seDays')));
  CheckResult(sePlaycount.SetValueAsObject(AIMPUI_SPINEDIT_PROPID_DISPLAYMASK,
    LangLoadStringEx('skiptrack_options\sePlaycount')));
  CheckResult(seArtists.SetValueAsObject(AIMPUI_SPINEDIT_PROPID_DISPLAYMASK,
    LangLoadStringEx('skiptrack_options\seArtists')));

  FMainLoc := LangLoadStringEx('skiptrack_options\General');
  FPlsLoc := LangLoadStringEx('skiptrack_options\Playlists');
end;

procedure TOptionsFrame.ConfigLoad;
var
  ACount: Integer;
  APlaylist: IAIMPPlaylist;
  APlaylistManager: IAIMPServicePlaylistManager;
  AMainSettings: TPlaylist;
begin
  FModified := True;
  // Список плейлистов
  if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
  begin
    try
      CheckResult(cbbPlaylists.Clear);
      FPlaylistSettings.Clear;
      // Общие настройки
      if Succeeded(cbbPlaylists.Add(nil, 0)) then
      begin
        AMainSettings := TPlaylist.Create;
        AMainSettings.Playlist := nil;
        AMainSettings.OverrideSettings := True;
        AMainSettings.PlstFilter := FPlaybackQueue.Filter;
        AMainSettings.PlsArtists := FPlaybackQueue.Artists;
        AMainSettings.PlsDays := FPlaybackQueue.Days;
        AMainSettings.PlsPlaycount := FPlaybackQueue.Playcount;
        AMainSettings.PlsExcept := FPlaybackQueue.ExceptEnable;
        AMainSettings.PlsExceptFav := FPlaybackQueue.ExceptFav;
        AMainSettings.PlsQueueSave := FPlaybackQueue.QueueSave;
        FPlaylistSettings.Add(0, AMainSettings);
        cbbPlaylists.SetValueAsInt32(AIMPUI_COMBOBOX_PROPID_ITEMINDEX, 0);
        HandlerComboBox(cbbPlaylists);
        FForm.Localize;
      end;
      // Настройки плейлистов
      for ACount := 0 to APlaylistManager.GetLoadedPlaylistCount - 1 do
      begin
        if Succeeded(APlaylistManager.GetLoadedPlaylist(ACount, APlaylist)) then
        begin
          FPlaybackQueue.CreatePlaylist(APlaylist);
          cbbPlaylists.Add(MakeString(GetPlaylistName(APlaylist)), 0);
          FPlaylistSettings.AddPlaylist(ACount + 1, APlaylist);
        end;
      end;
    finally
      APlaylistManager := nil;
      APlaylist := nil;
    end;
  end;
  // Исключения
  CheckResult(tlExceptions.Clear);
  for ACount := 0 to FExceptions.Count - 1 do
    AddArtist(FExceptions[ACount]);
  SelectFirstNode;
  OnStructChanged(tlExceptions);
end;

procedure TOptionsFrame.ConfigSave;
var
  ACount: Integer;
  APlaylist: IAIMPPlaylist;
  ANode, ARootNode: IAIMPUITreeListNode;
  AString: IAIMPString;
begin
  // Общие настройки
  FPlaybackQueue.Filter := FPlaylistSettings[0].PlstFilter;
  FPlaybackQueue.Days := FPlaylistSettings[0].PlsDays;
  FPlaybackQueue.Playcount := FPlaylistSettings[0].PlsPlaycount;
  FPlaybackQueue.Artists := FPlaylistSettings[0].PlsArtists;
  FPlaybackQueue.QueueSave := FPlaylistSettings[0].PlsQueueSave;
  FPlaybackQueue.ExceptEnable := FPlaylistSettings[0].PlsExcept;
  FPlaybackQueue.ExceptFav := FPlaylistSettings[0].PlsExceptFav;
  // Настройки плейлистов
  for ACount := 1 to FPlaylistSettings.Count - 1 do
  begin
    APlaylist := FPlaylistSettings[ACount].Playlist;
    if FPlaybackQueue.ContainsKey(APlaylist) then
      with FPlaybackQueue[APlaylist] do
      begin
        NoMathSongList.Clear;
        OverrideSettings := FPlaylistSettings[ACount].OverrideSettings;
        PlstFilter := FPlaylistSettings[ACount].PlstFilter;
        PlsDays := FPlaylistSettings[ACount].PlsDays;
        PlsPlaycount := FPlaylistSettings[ACount].PlsPlaycount;
        PlsArtists := FPlaylistSettings[ACount].PlsArtists;
        PlsQueueSave := FPlaylistSettings[ACount].PlsQueueSave;
        PlsExcept := FPlaylistSettings[ACount].PlsExcept;
        PlsExceptFav := FPlaylistSettings[ACount].PlsExceptFav;
      end;
  end;
  // Исключения
  if Succeeded(tlExceptions.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
  begin
    FExceptions.Clear;
    for ACount := 0 to ARootNode.GetCount - 1 do
      if Succeeded(ARootNode.Get(ACount, IID_IAIMPUITreeListNode, ANode)) then
        if Succeeded(ANode.GetValue(0, AString)) then
          FExceptions.Add(IAIMPStringToString(AString));
    FExceptions.SaveToFile(CoreGetProfilePath + '\exceptions.txt', TEncoding.UTF8);
  end;
end;

procedure TOptionsFrame.DoModified;
begin
  if Assigned(OnModified) then
    OnModified(self);
end;

constructor TOptionsFrame.Create(Parent: HWND; AService: IAIMPServiceUI);
var
  AControl: IAIMPUIWinControl;
begin
  FPlaylistSettings := TPlaylistSettings.Create([doOwnsValues]);
  FMainLoc := LangLoadStringEx('skiptrack_options\General');
  FPlsLoc := LangLoadStringEx('skiptrack_options\Playlists');
  FService := AService;

  if Succeeded(AService.CreateForm(Parent, AIMPUI_SERVICE_CREATEFORM_FLAGS_CHILD, MakeString('skiptrack_options'), self,
    FForm)) then
  begin
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_BORDERSTYLE, AIMPUI_FLAGS_BORDERSTYLE_NONE));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_BORDERICONS, 0));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_PADDING, 0));

    // Main Category
    if Succeeded(FService.CreateControl(FForm, FForm, MakeString('Main'), self, IID_IAIMPUICategory, AControl)) then
    begin
      CheckResult(AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualClient, 0, NullRect)));

      CreatePlaylist(AControl);
      CreateExceptions(AControl);
    end;
  end;
end;

procedure TOptionsFrame.CreateLabel(Parent: IAIMPUIWinControl; const Name: IAIMPString; const Bounds: TRect);
var
  AControl: IAIMPUIWinControl;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, nil, IAIMPUILabel, AControl)) then
  begin
    CheckResult(AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds)));
    CheckResult(AControl.SetValueAsInt32(AIMPUI_LABEL_PROPID_AUTOSIZE, 1));
  end;
end;

function TOptionsFrame.CreateCheckBox(Parent: IAIMPUIWinControl; const Name: IAIMPString; EventsHandler: IInterface;
  const Bounds: TRect): IAIMPUICheckBox;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, EventsHandler, IID_IAIMPUICheckBox, Result)) then
  begin
    CheckResult(Result.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds)));
    CheckResult(Result.SetValueAsInt32(AIMPUI_CHECKBOX_PROPID_AUTOSIZE, 1));
  end;
end;

function TOptionsFrame.CreateSpinEdit(Parent: IAIMPUIWinControl; const Name: IAIMPString; EventsHandler: IInterface;
  const Bounds: TRect; MaskKey: string): IAIMPUISpinEdit;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, EventsHandler, IAIMPUISpinEdit, Result)) then
  begin
    CheckResult(Result.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds)));
    CheckResult(Result.SetValueAsObject(AIMPUI_SPINEDIT_PROPID_DISPLAYMASK,
      LangLoadStringEx('skiptrack_options\' + MaskKey)));
    CheckResult(Result.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_INCREMENT, 1));
    CheckResult(Result.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_MAXVALUE, 9999));
    CheckResult(Result.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_MINVALUE, 0));
  end;
end;

function TOptionsFrame.CreateTreeList(Parent: IAIMPUIWinControl; const Name: IAIMPString;
  Placement: TAIMPUIControlPlacement): IAIMPUITreeList;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, self, IAIMPUITreeList, Result)) then
  begin
    CheckResult(Result.SetPlacement(Placement));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_AUTOWIDTH, 1));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_VISIBLE, 0));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_HOT_TRACK, 1));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_AUTOWIDTH, 1));
  end;
end;

procedure TOptionsFrame.ExceptState(const Index: Integer);
var
  AState: Integer;
begin
  AState := Disable;

  if FPlaylistSettings[Index].OverrideSettings then
  begin
    CheckResult(gbExcept.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, Enable));
    if PropListGetInt32A(gbExcept, AIMPUI_GROUPBOX_PROPID_CHECKED) <> 0 then
      AState := Enable;
  end
  else
  begin
    AState := Disable;
    CheckResult(gbExcept.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
  end;

  CheckResult(cbFav.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
  CheckResult(edArtist.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
  CheckResult(tlExceptions.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
  CheckResult(btnAdd.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
  CheckResult(btnDelete.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, AState));
end;

procedure TOptionsFrame.HandlerComboBox(const Sender: IInterface);
var
  AIndex: Integer;
begin
  FModified := False;
  try
    cbbPlaylists.GetValueAsInt32(AIMPUI_COMBOBOX_PROPID_ITEMINDEX, AIndex);
    if AIndex = -1 then
      Exit;

    SetFilter(FPlaylistSettings[AIndex].PlstFilter);
    SpinEditState;
    CheckResult(seDays.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_VALUE, FPlaylistSettings[AIndex].PlsDays));
    CheckResult(sePlaycount.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_VALUE, FPlaylistSettings[AIndex].PlsPlaycount));
    CheckResult(seArtists.SetValueAsInt32(AIMPUI_SPINEDIT_PROPID_VALUE, FPlaylistSettings[AIndex].PlsArtists));
    SetCheckBoxState(cbQueueSave, FPlaylistSettings[AIndex].PlsQueueSave.ToInteger);
    CheckResult(gbExcept.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKED,
      FPlaylistSettings[AIndex].PlsExcept.ToInteger));
    CheckResult(gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKED,
      FPlaylistSettings[AIndex].OverrideSettings.ToInteger));
    SetCheckBoxState(cbFav, FPlaylistSettings[AIndex].PlsExceptFav.ToInteger);

    if AIndex = 0 then
    begin
      gbPlaylists.SetValueAsObject(AIMPUI_GROUPBOX_PROPID_CAPTION, FMainLoc);
      gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKMODE, 0);
    end
    else
    begin
      gbPlaylists.SetValueAsObject(AIMPUI_GROUPBOX_PROPID_CAPTION, FPlsLoc);
      gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKMODE, 2);
    end;

    ExceptState(AIndex);
  finally
    FModified := True;
  end;
end;

procedure TOptionsFrame.AddArtist(const Artist: string);
var
  ANode: IAIMPUITreeListNode;
  ARootNode: IAIMPUITreeListNode;
begin
  if Succeeded(tlExceptions.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
    if Succeeded(ARootNode.Add(ANode)) then
      CheckResult(ANode.SetValue(0, MakeString(Artist)));
end;

procedure TOptionsFrame.CreatePlaylist(AParent: IAIMPUIWinControl);
begin
  // Playlist List ComboBox
  if Succeeded(FService.CreateControl(FForm, AParent, MakeString('cbbPlaylists'),
    TAIMPUINotifyEventAdapter.Create(HandlerComboBox), IID_IAIMPUIComboBox, cbbPlaylists)) then
  begin
    CheckResult(cbbPlaylists.SetPlacement(TAIMPUIControlPlacement.Create(ualTop, 0)));
    CheckResult(cbbPlaylists.SetValueAsInt32(AIMPUI_COMBOBOX_PROPID_STYLE, 1));
  end;

  // Playlist Settings GroupBox
  if Succeeded(FService.CreateControl(FForm, AParent, MakeString('gbPlaylists'), self, IID_IAIMPUIGroupBox, gbPlaylists))
  then
  begin
    CheckResult(gbPlaylists.SetPlacement(TAIMPUIControlPlacement.Create(ualTop, 0)));
    CheckResult(gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKMODE, 2));
    CheckResult(gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_TRANSPARENT, 1));
    CheckResult(gbPlaylists.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_AUTOSIZE, 1));
    AParent := gbPlaylists;

    // Filter Label
    CreateLabel(AParent, MakeString('lbFilter'), Bounds(9, 9, 0, 0));
    // Days CheckBox
    cbDays := CreateCheckBox(AParent, MakeString('cbDays'), self, Bounds(27, 30, 260, 0));
    // Days SpinEdit
    seDays := CreateSpinEdit(AParent, MakeString('seDays'), self, Bounds(320, 30, 150, 0), 'seDays');
    // Playcount CheckBox
    cbPlaycount := CreateCheckBox(AParent, MakeString('cbPlaycount'), self, Bounds(27, 52, 260, 0));
    // Playcount SpinEdit
    sePlaycount := CreateSpinEdit(AParent, MakeString('sePlaycount'), self, Bounds(320, 52, 150, 0), 'sePlaycount');
    // Artists CheckBox
    cbArtists := CreateCheckBox(AParent, MakeString('cbArtists'), self, Bounds(27, 74, 260, 0));
    // Artists SpinEdit
    seArtists := CreateSpinEdit(AParent, MakeString('seArtists'), self, Bounds(320, 74, 150, 0), 'seArtists');
    // QueueSave CheckBox
    cbQueueSave := CreateCheckBox(AParent, MakeString('cbQueueSave'), self, Bounds(9, 98, 260, 0));
  end;
end;

procedure TOptionsFrame.CreateExceptions(AParent: IAIMPUIWinControl);
var
  AColumn: IAIMPUITreeListColumn;
begin
  // Exceptions GroupBox
  if Succeeded(FService.CreateControl(FForm, AParent, MakeString('gbExcept'), self, IAIMPUIGroupBox, gbExcept)) then
  begin
    CheckResult(gbExcept.SetPlacement(TAIMPUIControlPlacement.Create(ualTop, 0)));
    CheckResult(gbExcept.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_CHECKMODE, 2));
    CheckResult(gbExcept.SetValueAsInt32(AIMPUI_GROUPBOX_PROPID_AUTOSIZE, 1));
    AParent := gbExcept;
    // Fav CheckBox
    cbFav := CreateCheckBox(AParent, MakeString('cbFav'), self, Bounds(9, 25, 375, 0));
    // Artist Edit
    if Succeeded(FService.CreateControl(FForm, AParent, MakeString('edArtist'), self, IAIMPUIEdit, edArtist)) then
      CheckResult(edArtist.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds(9, 49, 375, 0))));
    // Exceptions TreeList
    tlExceptions := CreateTreeList(AParent, MakeString('tlExceptions'), TAIMPUIControlPlacement.Create(ualNone,
      Bounds(9, 80, 375, 163)));
    if Assigned(tlExceptions) then
    begin
      CheckResult(tlExceptions.SetValueAsInt32(AIMPUI_TL_PROPID_ALLOW_MULTISELECT, 1));
      // Artists Column
      CheckResult(tlExceptions.AddColumn(IAIMPUITreeListColumn, AColumn));
    end;
    // Exceptions Add Button
    if Succeeded(FService.CreateControl(FForm, AParent, MakeString('btnAdd'), self, IID_IAIMPUIButton, btnAdd)) then
    begin
      CheckResult(btnAdd.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds(395, 47, 75, 25))));
      CheckResult(btnAdd.SetValueAsInt32(AIMPUI_BUTTON_PROPID_FOCUSONCLICK, 0));
    end;

    // Exceptions Delete Button
    if Succeeded(FService.CreateControl(FForm, AParent, MakeString('btnDelete'), self, IID_IAIMPUIButton, btnDelete))
    then
      CheckResult(btnDelete.SetPlacement(TAIMPUIControlPlacement.Create(ualNone, Bounds(395, 80, 75, 25))));
  end;
end;

procedure TOptionsFrame.SetFilter(const Filter: Integer);
begin
  case Filter of
    0:
      begin
        SetCheckBoxState(cbDays, Disable);
        SetCheckBoxState(cbPlaycount, Disable);
        SetCheckBoxState(cbArtists, Disable);
      end;
    1:
      begin
        SetCheckBoxState(cbDays, Enable);
        SetCheckBoxState(cbPlaycount, Disable);
        SetCheckBoxState(cbArtists, Disable);
      end;
    2:
      begin
        SetCheckBoxState(cbDays, Disable);
        SetCheckBoxState(cbPlaycount, Enable);
        SetCheckBoxState(cbArtists, Disable);
      end;
    3:
      begin
        SetCheckBoxState(cbDays, Enable);
        SetCheckBoxState(cbPlaycount, Enable);
        SetCheckBoxState(cbArtists, Disable);
      end;
    4:
      begin
        SetCheckBoxState(cbDays, Disable);
        SetCheckBoxState(cbPlaycount, Disable);
        SetCheckBoxState(cbArtists, Enable);
      end;
    5:
      begin
        SetCheckBoxState(cbDays, Enable);
        SetCheckBoxState(cbPlaycount, Disable);
        SetCheckBoxState(cbArtists, Enable);
      end;
    6:
      begin
        SetCheckBoxState(cbDays, Disable);
        SetCheckBoxState(cbPlaycount, Enable);
        SetCheckBoxState(cbArtists, Enable);
      end;
    7:
      begin
        SetCheckBoxState(cbDays, Enable);
        SetCheckBoxState(cbPlaycount, Enable);
        SetCheckBoxState(cbArtists, Enable);
      end;
  end;
end;

function TOptionsFrame.GetFilter: Integer;
var
  Days, Playcount, Artists: Boolean;
begin
  Result := 0;

  if PropListGetBool(cbDays, AIMPUI_CHECKBOX_PROPID_STATE, Days) and
    PropListGetBool(cbPlaycount, AIMPUI_CHECKBOX_PROPID_STATE, Playcount) and
    PropListGetBool(cbArtists, AIMPUI_CHECKBOX_PROPID_STATE, Artists) then
  begin
    if (not Days) and (not Playcount) and (not Artists) then
      Result := 0;
    if Days and (not Playcount) and (not Artists) then
      Result := 1;
    if (not Days) and Playcount and (not Artists) then
      Result := 2;
    if Days and Playcount and (not Artists) then
      Result := 3;
    if (not Days) and (not Playcount) and Artists then
      Result := 4;
    if Days and (not Playcount) and Artists then
      Result := 5;
    if (not Days) and Playcount and Artists then
      Result := 6;
    if Days and Playcount and Artists then
      Result := 7;
  end;
end;

procedure TOptionsFrame.OnChanged(Sender: IInterface);
var
  AIntf: IAIMPUIControl;
  AString: IAIMPString;
  AControlName: string;
  AIndex: Integer;
begin
  if not FModified then
    Exit;

  if Supports(Sender, IAIMPUIControl, AIntf) then
    if PropListGetStr(AIntf, AIMPUI_CONTROL_PROPID_NAME, AControlName) then
    begin
      if AControlName = 'btnAdd' then
      begin
        if PropListGetStr(edArtist, AIMPUI_BASEEDIT_PROPID_TEXT, AString) then
          if AString.GetLength > 0 then
          begin
            AddArtist(IAIMPStringToString(AString));
            edArtist.SetValueAsObject(AIMPUI_BASEEDIT_PROPID_TEXT, nil);
            DoModified;
          end;
        Exit;
      end;

      if AControlName = 'btnDelete' then
        if Succeeded(tlExceptions.DeleteSelected) then
        begin
          DoModified;
          Exit;
        end;

      cbbPlaylists.GetValueAsInt32(AIMPUI_COMBOBOX_PROPID_ITEMINDEX, AIndex);
      if AIndex = -1 then
        Exit;

      if (AControlName = 'cbDays') or (AControlName = 'cbPlaycount') or (AControlName = 'cbArtists') then
      begin
        FPlaylistSettings[AIndex].PlstFilter := GetFilter;
        SpinEditState;
        DoModified;
        Exit;
      end;

      if AControlName = 'gbExcept' then
      begin
        FPlaylistSettings[AIndex].PlsExcept := PropListGetInt32A(gbExcept, AIMPUI_GROUPBOX_PROPID_CHECKED).ToBoolean;
        OnStructChanged(tlExceptions);
        DoModified;
        Exit;
      end;

      if AControlName = 'gbPlaylists' then
      begin
        FPlaylistSettings[AIndex].OverrideSettings := PropListGetInt32A(gbPlaylists, AIMPUI_GROUPBOX_PROPID_CHECKED)
          .ToBoolean;
        ExceptState(AIndex);
        DoModified;
        Exit;
      end;

      if AControlName = 'cbFav' then
      begin
        FPlaylistSettings[AIndex].PlsExceptFav := PropListGetInt32A(cbFav, AIMPUI_CHECKBOX_PROPID_STATE).ToBoolean;
        DoModified;
        Exit;
      end;

      if AControlName = 'cbQueueSave' then
      begin
        FPlaylistSettings[AIndex].PlsQueueSave := PropListGetInt32A(cbQueueSave, AIMPUI_CHECKBOX_PROPID_STATE)
          .ToBoolean;
        DoModified;
        Exit;
      end;

      if AControlName = 'seDays' then
      begin
        FPlaylistSettings[AIndex].PlsDays := PropListGetInt32A(seDays, AIMPUI_SPINEDIT_PROPID_VALUE);
        DoModified;
        Exit;
      end;

      if AControlName = 'sePlaycount' then
      begin
        FPlaylistSettings[AIndex].PlsPlaycount := PropListGetInt32A(sePlaycount, AIMPUI_SPINEDIT_PROPID_VALUE);
        DoModified;
        Exit;
      end;

      if AControlName = 'seArtists' then
      begin
        FPlaylistSettings[AIndex].PlsArtists := PropListGetInt32A(seArtists, AIMPUI_SPINEDIT_PROPID_VALUE);
        DoModified;
      end;
    end;
end;

procedure TOptionsFrame.OnStructChanged(Sender: IAIMPUITreeList);
var
  ARootNode: IAIMPUITreeListNode;
begin
  if not Assigned(btnDelete) then
    Exit;

  if Succeeded(tlExceptions.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
    if ARootNode.GetCount <= 0 then
      CheckResult(btnDelete.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, Disable))
    else if PropListGetInt32A(gbExcept, AIMPUI_GROUPBOX_PROPID_CHECKED) <> 0 then
      CheckResult(btnDelete.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, Enable));
end;

procedure TOptionsFrame.SpinEditState;
begin
  if PropListGetInt32A(gbPlaylists, AIMPUI_GROUPBOX_PROPID_CHECKED) <> 0 then
  begin
    CheckResult(seDays.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, PropListGetInt32A(cbDays,
      AIMPUI_CHECKBOX_PROPID_STATE)));
    CheckResult(sePlaycount.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, PropListGetInt32A(cbPlaycount,
      AIMPUI_CHECKBOX_PROPID_STATE)));
    CheckResult(seArtists.SetValueAsInt32(AIMPUI_CONTROL_PROPID_ENABLED, PropListGetInt32A(cbArtists,
      AIMPUI_CHECKBOX_PROPID_STATE)));
  end;
end;

procedure TOptionsFrame.OnKeyDown(Sender: IInterface; var Key: Word; Modifiers: Word);
var
  AIntf: IAIMPUIControl;
  AControlName: string;
begin
  if Supports(Sender, IAIMPUIControl, AIntf) then
    if PropListGetStr(AIntf, AIMPUI_CONTROL_PROPID_NAME, AControlName) then
    begin
      if (AControlName = 'edArtist') and (Key = VK_RETURN) then
        OnChanged(btnAdd);

      if (AControlName = 'tlExceptions') and (Key = VK_DELETE) then
        OnChanged(btnDelete);
    end;
end;

procedure TOptionsFrame.SelectFirstNode;
var
  ANode: IAIMPUITreeListNode;
  ARootNode: IAIMPUITreeListNode;
begin
  if Succeeded(tlExceptions.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
  begin
    if ARootNode.GetCount <= 0 then
      Exit;

    if Succeeded(ARootNode.Get(0, IID_IAIMPUITreeListNode, ANode)) then
      if Succeeded(tlExceptions.SetFocused(ANode)) then
        if Succeeded(tlExceptions.MakeTop(ANode)) then
  end;
end;

procedure TOptionsFrame.SetCheckBoxState(Name: IAIMPUICheckBox; const State: Integer);
begin
  CheckResult(Name.SetValueAsInt32(AIMPUI_CHECKBOX_PROPID_STATE, State));
end;

procedure TOptionsFrame.OnDestroyed(Sender: IAIMPUIForm);
begin
  cbbPlaylists := nil;
  gbPlaylists := nil;
  cbDays := nil;
  cbPlaycount := nil;
  cbArtists := nil;
  cbQueueSave := nil;
  seDays := nil;
  sePlaycount := nil;
  seArtists := nil;
  gbExcept := nil;
  cbFav := nil;
  edArtist := nil;
  tlExceptions := nil;
  btnAdd := nil;
  btnDelete := nil;

  FreeAndNil(FPlaylistSettings);
  FForm := nil;
end;

{$REGION 'do nothing'}

procedure TOptionsFrame.OnKeyPress(Sender: IInterface; var Key: WideChar);
begin
  // do nothing
end;

procedure TOptionsFrame.OnKeyUp(Sender: IInterface; var Key: Word; Modifiers: Word);
begin
  // do nothing
end;

procedure TOptionsFrame.OnShortCut(Sender: IAIMPUIForm; Key, Modifiers: Word; var Handled: LongBool);
begin
  // do nothing
end;

procedure TOptionsFrame.OnEnter(Sender: IInterface);
begin
  // do nothing
end;

procedure TOptionsFrame.OnExit(Sender: IInterface);
begin
  // do nothing
end;

procedure TOptionsFrame.OnSelectionChanged(Sender: IAIMPUITreeList);
begin
  // do nothing
end;

procedure TOptionsFrame.OnSorted(Sender: IAIMPUITreeList);
begin
  // do nothing
end;

procedure TOptionsFrame.OnActivated(Sender: IAIMPUIForm);
begin
  // do nothing
end;

procedure TOptionsFrame.OnCloseQuery(Sender: IAIMPUIForm; var CanClose: LongBool);
begin
  // do nothing
end;

procedure TOptionsFrame.OnColumnClick(Sender: IAIMPUITreeList; ColumnIndex: Integer);
begin
  // do nothing
end;

procedure TOptionsFrame.OnCreated(Sender: IAIMPUIForm);
begin
  // do nothing
end;

procedure TOptionsFrame.OnDeactivated(Sender: IAIMPUIForm);
begin
  // do nothing
end;

procedure TOptionsFrame.OnLocalize(Sender: IAIMPUIForm);
begin
  // do nothing
end;

procedure TOptionsFrame.OnNodeChecked(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode);
begin
  // do nothing
end;

procedure TOptionsFrame.OnNodeDblClicked(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode);
begin
  // do nothing
end;

procedure TOptionsFrame.OnFocusedColumnChanged(Sender: IAIMPUITreeList);
begin
  // do nothing
end;

procedure TOptionsFrame.OnFocusedNodeChanged(Sender: IAIMPUITreeList);
begin
  // do nothing
end;
{$ENDREGION}

end.
