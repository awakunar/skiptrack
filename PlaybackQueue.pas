﻿unit PlaybackQueue;

interface

uses apiPlaylists, System.Generics.Collections;

type
  TPlaylist = class
    SongItem: TList<IAIMPPlaylistItem>;
    NoMathSongList: TList<IAIMPPlaylistItem>;
    LastArtist: TQueue<string>;
    PlaybackCursor: Integer;
    PlaylistEnd: Boolean;
    OverrideSettings: Boolean;
    PlstFilter: Integer;
    PlsArtists: Integer;
    PlsDays: Integer;
    PlsPlaycount: Integer;
    PlsExcept: Boolean;
    PlsExceptFav: Boolean;
    PlsQueueSave: Boolean;
  public
    destructor Destroy; override;
  end;

  TPlaybackQueue = class(TObjectDictionary<IAIMPPlaylist, TPlaylist>)
  private
    FPlaylist: TPlaylist;
  public
    ExceptEnable: Boolean;
    ExceptFav: Boolean;
    QueueSave: Boolean;
    Filter: Integer;
    Days: Integer;
    Artists: Integer;
    Playcount: Integer;
    procedure CreatePlaylist(const Playlist: IAIMPPlaylist);
    procedure RemoveDeletedItems(const Playlist: IAIMPPlaylist);
    procedure ClearPlaylist(const Playlist: IAIMPPlaylist);
    procedure LoadFromXML(const FileName: string);
    procedure SaveToXML(const FileName: string);
  end;

implementation

uses Defines, ActiveX, System.Sysutils, XMLIntf, XMLDoc, apiObjects,
  apiWrappers;

destructor TPlaylist.Destroy;
begin
  FreeAndNil(SongItem);
  FreeAndNil(NoMathSongList);
  FreeAndNil(LastArtist);

  inherited;
end;

procedure TPlaybackQueue.CreatePlaylist(const Playlist: IAIMPPlaylist);
begin
  if not self.ContainsKey(Playlist) then
  begin
    FPlaylist := TPlaylist.Create;
    with FPlaylist do
    begin
      SongItem := TList<IAIMPPlaylistItem>.Create;
      NoMathSongList := TList<IAIMPPlaylistItem>.Create;
      LastArtist := TQueue<string>.Create;
      PlaybackCursor := -1;
      PlaylistEnd := False;
      OverrideSettings := False;
      PlsDays := self.Days;
      PlsExcept := self.ExceptEnable;
      PlsExceptFav := self.ExceptFav;
      PlsQueueSave := self.QueueSave;
      PlstFilter := self.Filter;
      PlsArtists := self.Artists;
      PlsPlaycount := self.Playcount;
    end;
    self.Add(Playlist, FPlaylist);
  end;
end;

procedure TPlaybackQueue.ClearPlaylist(const Playlist: IAIMPPlaylist);
begin
  if not FPlaybackQueue.ContainsKey(Playlist) then
    Exit;

  with FPlaybackQueue[Playlist] do
  begin
    SongItem.Clear;
    NoMathSongList.Clear;
    LastArtist.Clear;
    PlaybackCursor := -1;
    PlaylistEnd := False;
  end;
end;

procedure TPlaybackQueue.RemoveDeletedItems(const Playlist: IAIMPPlaylist);
var
  AItemIndex, ACount: Integer;
  ATempPlaylist: IAIMPPlaylist;
  {$IFDEF DEBUG} ADelCount: Integer; {$ENDIF}
begin
  if self.ContainsKey(Playlist) then
  begin
    with self[Playlist] do
    begin
      if SongItem.Count <= 0 then
        Exit;

      ACount := 0;
      {$IFDEF DEBUG} ADelCount := 0; {$ENDIF}
      while ACount < SongItem.Count - 1 do
      begin
        Inc(ACount);
        if SongItem[ACount] <> nil then
        begin
          if Failed(SongItem[ACount].GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_PLAYLIST, IID_IAIMPPlaylist,
            ATempPlaylist)) then
          begin
            SongItem.Delete(ACount);
            Dec(ACount);

            if PlaybackCursor > ACount then
              if PlaybackCursor > 0 then
                Dec(PlaybackCursor);

            {$IFDEF DEBUG}Inc(ADelCount); {$ENDIF}
          end;
        end
        else
          SongItem.Delete(ACount);
      end;

      if SongItem[0] <> nil then
      begin
        if Failed(SongItem[0].GetValueAsInt32(AIMP_PLAYLISTITEM_PROPID_INDEX, AItemIndex)) then
        begin
          SongItem.Delete(0);

          if PlaybackCursor > -1 then
            Dec(PlaybackCursor);
        end;
      end
      else
      begin
        SongItem.Delete(0);
        if PlaybackCursor > -1 then
          Dec(PlaybackCursor);
        PlaylistEnd := False;
      end;
      SongItem.TrimExcess;

      {$IFDEF DEBUG}
      if ADelCount > 0 then
        if Assigned(FInfoForm.Form) then
          FInfoForm.DebugMessageS(GetPlaylistName(Playlist), 'Удалено ' + ADelCount.ToString + ' песен', STYLE_NOTICE);
      {$ENDIF}
    end;
  end;
end;

procedure TPlaybackQueue.LoadFromXML(const FileName: string);
var
  APlaylist: IXMLNode;
  AItemList: IXMLNode;
  XMLDoc: IXMLDocument;
  ACount, ANode: Integer;
  AKey: IAIMPPlaylist;
  APlaylistItem: IAIMPPlaylistItem;
begin
  XMLDoc := TXMLDocument.Create(nil);
  try
    if FileExists(FileName) then
      XMLDoc.LoadFromFile(FileName)
    else
      Exit;

    for ANode := 0 to XMLDoc.DocumentElement.ChildNodes.Count - 1 do
    begin
      APlaylist := XMLDoc.DocumentElement.ChildNodes[ANode];
      AKey := GetPlaylistByID(APlaylist.ChildNodes['id'].Text);
      if AKey <> nil then
      begin
        self.CreatePlaylist(AKey);

        if APlaylist.ChildNodes['override'].Text <> EmptyStr then
          self[AKey].OverrideSettings := APlaylist.ChildNodes['override'].Text.ToBoolean;
        if APlaylist.ChildNodes['filter'].Text <> EmptyStr then
          self[AKey].PlstFilter := APlaylist.ChildNodes['filter'].Text.ToInteger;
        if APlaylist.ChildNodes['days'].Text <> EmptyStr then
          self[AKey].PlsDays := APlaylist.ChildNodes['days'].Text.ToInteger;
        if APlaylist.ChildNodes['playcount'].Text <> EmptyStr then
          self[AKey].PlsPlaycount := APlaylist.ChildNodes['playcount'].Text.ToInteger;
        if APlaylist.ChildNodes['artists'].Text <> EmptyStr then
          self[AKey].PlsArtists := APlaylist.ChildNodes['artists'].Text.ToInteger;
        if APlaylist.ChildNodes['except'].Text <> EmptyStr then
          self[AKey].PlsExcept := APlaylist.ChildNodes['except'].Text.ToBoolean;
        if APlaylist.ChildNodes['fav'].Text <> EmptyStr then
          self[AKey].PlsExceptFav := APlaylist.ChildNodes['fav'].Text.ToBoolean;
        if APlaylist.ChildNodes['queue'].Text <> EmptyStr then
          self[AKey].PlsQueueSave := APlaylist.ChildNodes['queue'].Text.ToBoolean;

        if self[AKey].OverrideSettings then
        begin
          if self[AKey].PlsQueueSave then
          begin
            self[AKey].PlaybackCursor := APlaylist.ChildNodes['cursor'].Text.ToInteger;
            AItemList := APlaylist.ChildNodes['items'];
            for ACount := 0 to AItemList.ChildNodes.Count - 1 do
              if Succeeded(AKey.GetItem(AItemList.ChildNodes[ACount].Text.ToInteger, IID_IAIMPPlaylistItem,
                APlaylistItem)) then
                self[AKey].SongItem.Add(APlaylistItem);
            AItemList := APlaylist.ChildNodes['artist'];
            for ACount := 0 to AItemList.ChildNodes.Count - 1 do
              self[AKey].LastArtist.Enqueue(AItemList.ChildNodes[ACount].Text);
          end;
        end
        else if self.QueueSave then
        begin
          self[AKey].PlaybackCursor := APlaylist.ChildNodes['cursor'].Text.ToInteger;
          AItemList := APlaylist.ChildNodes['items'];
          for ACount := 0 to AItemList.ChildNodes.Count - 1 do
            if Succeeded(AKey.GetItem(AItemList.ChildNodes[ACount].Text.ToInteger, IID_IAIMPPlaylistItem, APlaylistItem))
            then
              self[AKey].SongItem.Add(APlaylistItem);
          AItemList := APlaylist.ChildNodes['artist'];
          for ACount := 0 to AItemList.ChildNodes.Count - 1 do
            self[AKey].LastArtist.Enqueue(AItemList.ChildNodes[ACount].Text);
        end;
      end;
    end;
  finally
    AKey := nil;
    APlaylistItem := nil;
    AItemList := nil;
    APlaylist := nil;
    XMLDoc := nil;
  end;
end;

procedure TPlaybackQueue.SaveToXML(const FileName: string);
var
  XMLDoc: IXMLDocument;
  APlaylist: IXMLNode;
  AValueNode: IXMLNode;
  AItemList: IXMLNode;
  AKey: IAIMPPlaylist;
  ACount, AItemIndex: Integer;
  AArtist: string;
begin
  XMLDoc := TXMLDocument.Create(nil);
  try
    XMLDoc.Active := True;
    XMLDoc.Encoding := 'UTF-8';
    XMLDoc.DocumentElement := XMLDoc.CreateNode('PlaylistQueue', ntElement, '');

    for AKey in self.Keys do
    begin
      if not self[AKey].PlaylistEnd then
      begin
        APlaylist := XMLDoc.DocumentElement.AddChild('Playlist');
        AValueNode := APlaylist.AddChild('id');
        AValueNode.Text := GetPlaylistID(AKey);
        AValueNode := APlaylist.AddChild('override');
        AValueNode.Text := self[AKey].OverrideSettings.ToString;
        AValueNode := APlaylist.AddChild('filter');
        AValueNode.Text := self[AKey].PlstFilter.ToString;
        AValueNode := APlaylist.AddChild('days');
        AValueNode.Text := self[AKey].PlsDays.ToString;
        AValueNode := APlaylist.AddChild('playcount');
        AValueNode.Text := self[AKey].PlsPlaycount.ToString;
        AValueNode := APlaylist.AddChild('artists');
        AValueNode.Text := self[AKey].PlsArtists.ToString;
        AValueNode := APlaylist.AddChild('except');
        AValueNode.Text := self[AKey].PlsExcept.ToString;
        AValueNode := APlaylist.AddChild('fav');
        AValueNode.Text := self[AKey].PlsExceptFav.ToString;
        AValueNode := APlaylist.AddChild('queue');
        AValueNode.Text := self[AKey].PlsQueueSave.ToString;

        if self[AKey].OverrideSettings then
        begin
          if self[AKey].PlsQueueSave then
          begin
            AValueNode := APlaylist.AddChild('cursor');
            AValueNode.Text := self[AKey].PlaybackCursor.ToString;

            AValueNode := APlaylist.AddChild('items');
            for ACount := 0 to self.Items[AKey].SongItem.Count - 1 do
              if Succeeded(self[AKey].SongItem[ACount].GetValueAsInt32(AIMP_PLAYLISTITEM_PROPID_INDEX, AItemIndex)) then
              begin
                AItemList := AValueNode.AddChild('Item' + ACount.ToString);
                AItemList.Text := AItemIndex.ToString;
              end;

            AValueNode := APlaylist.AddChild('artist');
            ACount := 0;
            for AArtist in self[AKey].LastArtist do
            begin
              AItemList := AValueNode.AddChild('Item' + ACount.ToString);
              AItemList.Text := AArtist;
              Inc(ACount);
            end;
          end;
        end
        else if FPlaybackQueue.QueueSave then
        begin
          AValueNode := APlaylist.AddChild('cursor');
          AValueNode.Text := self[AKey].PlaybackCursor.ToString;

          AValueNode := APlaylist.AddChild('items');
          for ACount := 0 to self.Items[AKey].SongItem.Count - 1 do
            if Succeeded(self[AKey].SongItem[ACount].GetValueAsInt32(AIMP_PLAYLISTITEM_PROPID_INDEX, AItemIndex)) then
            begin
              AItemList := AValueNode.AddChild('Item' + ACount.ToString);
              AItemList.Text := AItemIndex.ToString;
            end;

          AValueNode := APlaylist.AddChild('artist');
          ACount := 0;
          for AArtist in self[AKey].LastArtist do
          begin
            AItemList := AValueNode.AddChild('Item' + ACount.ToString);
            AItemList.Text := AArtist;
            Inc(ACount);
          end;
        end;
      end;
    end;

    XMLDoc.SaveToFile(FileName);
  finally
    AItemList := nil;
    AValueNode := nil;
    APlaylist := nil;
    XMLDoc := nil;
  end;
end;

end.
