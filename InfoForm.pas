﻿unit InfoForm;

interface

{$R images.res}

uses
  Vcl.Graphics, Windows, Types, apiGUI, apiObjects, apiPlaylists,
  apiWrappersGUI, System.UITypes;

const
  NullRect: TRect = (Left: 0; Top: 0; Right: 0; Bottom: 0);

type
  TInfoForm = class(TInterfacedObject, IAIMPUIChangeEvents, IAIMPUIFormEvents, IAIMPUITreeListCustomDrawEvents)
  private
    imgTree: IAIMPUIImageList;
    imgButtons: IAIMPUIImageList;
    imgLog: IAIMPUIImageList;
    tlLog: IAIMPUITreeList;
    tlQueue: IAIMPUITreeList;
    btnClear: IAIMPUIButton;
    btnRefresh: IAIMPUIButton;
    btnFillRandom: IAIMPUIButton;
    btnFill: IAIMPUIButton;
    btnClearPls: IAIMPUIButton;
    btnClearAll: IAIMPUIButton;
    btnSettings: IAIMPUIButton;
    // IAIMPUIChangeEvents
    procedure OnChanged(Sender: IInterface); stdcall;
    // IAIMPUIFormEvents
    procedure OnActivated(Sender: IAIMPUIForm); overload; stdcall;
    procedure OnCloseQuery(Sender: IAIMPUIForm; var CanClose: LongBool); stdcall;
    procedure OnCreated(Sender: IAIMPUIForm); stdcall;
    procedure OnDeactivated(Sender: IAIMPUIForm); stdcall;
    procedure OnDestroyed(Sender: IAIMPUIForm); stdcall;
    procedure OnLocalize(Sender: IAIMPUIForm); stdcall;
    procedure OnShortCut(Sender: IAIMPUIForm; Key: Word; Modifiers: Word; var Handled: LongBool); stdcall;
    // IAIMPUITreeListCustomDrawEvents
    procedure OnCustomDrawNode(Sender: IAIMPUITreeList; DC: HDC; R: TRect; Node: IAIMPUITreeListNode;
      var Handled: LongBool); stdcall;
    procedure OnCustomDrawNodeCell(Sender: IAIMPUITreeList; DC: HDC; R: TRect; Node: IAIMPUITreeListNode;
      Column: IAIMPUITreeListColumn; var Handled: LongBool); stdcall;
    procedure OnGetNodeBackground(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode; var Color: DWORD); stdcall;
  protected
    FForm: IAIMPUIForm;
    FService: IAIMPServiceUI;
    procedure AddtoLog(const Caption, Item, Playcount, Lastplay, Reason, Style: string);
    procedure CreateHeader(AParent: IAIMPUIWinControl);
    procedure CreateLog(AParent: IAIMPUIWinControl);
    procedure CreateQueue(AParent: IAIMPUIWinControl);
    procedure CreateBottom(AParent: IAIMPUIWinControl);
  protected
    function CreateTreeList(Parent: IAIMPUIWinControl; const Name: IAIMPString; Placement: TAIMPUIControlPlacement)
      : IAIMPUITreeList;
    function CreateButton(Parent: IAIMPUIWinControl; const Name: IAIMPString; ImageIndex: Integer): IAIMPUIButton;
    function CreateImageList(ResName: PWideChar; ImageSize: Integer): IAIMPUIImageList;
  public
    constructor Create(AService: IAIMPServiceUI);
    procedure ShowPlaybackQueue;
    procedure DebugMessageG(const PlaylistItem: IAIMPPlaylistItem; Reason, Style: string);
    procedure DebugMessageO(const PlaylistItem: IAIMPPlaylistItem; Reason: string);
    procedure DebugMessageS(const Title, Reason, Style: string);
    property Form: IAIMPUIForm read FForm;
  end;

implementation

uses
  Defines, CheckSong, System.DateUtils, System.Generics.Collections,
  System.SysUtils, apiOptions, apiMessages, apiFileManager, apiWrappers;

{$REGION 'DebugMessages'}

procedure TInfoForm.DebugMessageG(const PlaylistItem: IAIMPPlaylistItem; Reason, Style: string);
var
  AFileInfo: IAIMPFileInfo;
  ADate: double;
  ASongDate: TDateTime;
begin
  try
    if Succeeded(PlaylistItem.GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_FILEINFO, IID_IAIMPFileInfo, AFileInfo)) then
    begin
      ADate := 0;
      AFileInfo.GetValueAsFloat(AIMP_FILEINFO_PROPID_STAT_LASTPLAYDATE, ADate);
      ASongDate := ADate;
      if ADate <> 0 then
        AddtoLog(PropListGetStr(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_DISPLAYTEXT),
          'Трек: ' + (PropListGetInt32A(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_INDEX) + 1).ToString,
          'Проигрываний: ' + PropListGetInt32A(AFileInfo, AIMP_FILEINFO_PROPID_STAT_PLAYCOUNT).ToString,
          'Проигрывался: ' + DateToStr(ASongDate) + ' (' + DaysBetween(date, ASongDate).ToString + ' дней назад)',
          Reason, Style)
      else
        AddtoLog(PropListGetStr(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_DISPLAYTEXT),
          'Трек: ' + (PropListGetInt32A(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_INDEX) + 1).ToString, 'Не проигрывался',
          Reason, '', Style);
    end;
  finally
    AFileInfo := nil;
  end;
end;

procedure TInfoForm.DebugMessageO(const PlaylistItem: IAIMPPlaylistItem; Reason: string);
begin
  AddtoLog(PropListGetStr(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_DISPLAYTEXT),
    'Трек: ' + (PropListGetInt32A(PlaylistItem, AIMP_PLAYLISTITEM_PROPID_INDEX) + 1).ToString, Reason, '', '',
    STYLE_SUCCESS);
end;

procedure TInfoForm.DebugMessageS(const Title, Reason, Style: string);
begin
  if Reason <> EmptyStr then
    AddtoLog(Title, Reason, '', '', '', Style);
end;

{$ENDREGION}

function CenterRect(const ABounds: TRect; AWidth, AHeight: Integer): TRect;
begin
  Result.Left := (ABounds.Left + ABounds.Right - AWidth) div 2;
  Result.Top := (ABounds.Top + ABounds.Bottom - AHeight) div 2;
  Result.Right := Result.Left + AWidth;
  Result.Bottom := Result.Top + AHeight;
end;

function TInfoForm.CreateImageList(ResName: PWideChar; ImageSize: Integer): IAIMPUIImageList;
begin
  if Succeeded(FService.CreateObject(FForm, nil, IAIMPUIImageList, Result)) then
  begin
    CheckResult(Result.SetSize(TSize.Create(ImageSize, ImageSize)));
    CheckResult(Result.LoadFromResource(HInstance, ResName, 'PNG'));
  end;
end;

constructor TInfoForm.Create(AService: IAIMPServiceUI);
var
  ABounds: TRect;
  AControl: IAIMPUIWinControl;
begin
  FService := AService;

  if Succeeded(AService.CreateForm(GetDesktopWindow, 0, MakeString('skiptrack_info'), Self, FForm)) then
  begin
    CheckResult(FForm.SetValueAsObject(AIMPUI_FORM_PROPID_CAPTION, MakeString('SkipTrack Info')));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_BORDERSTYLE, AIMPUI_FLAGS_BORDERSTYLE_SINGLE));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_BORDERICONS, AIMPUI_FLAGS_BORDERICON_SYSTEMMENU +
      AIMPUI_FLAGS_BORDERICON_MINIMIZE));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_CLOSEBYESCAPE, 1));
    CheckResult(FForm.SetValueAsInt32(AIMPUI_FORM_PROPID_SHOWONTASKBAR, 1));
    SystemParametersInfo(SPI_GETWORKAREA, 0, ABounds, 0);
    CheckResult(FForm.SetPlacement(TAIMPUIControlPlacement.Create(CenterRect(ABounds, 680, 500))));

    imgButtons := CreateImageList('BUTTONS', 32);
    imgLog := CreateImageList('LOG', 32);
    imgTree := CreateImageList('IMAGES', 24);

    // Main Category
    if Succeeded(FService.CreateControl(FForm, FForm, nil, nil, IAIMPUIPanel, AControl)) then
    begin
      CheckResult(AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualClient, 0, NullRect)));
      AControl.SetValueAsInt32(AIMPUI_PANEL_PROPID_TRANSPARENT, 1);
      AControl.SetValueAsInt32(AIMPUI_PANEL_PROPID_BORDERS, 0);

      CreateHeader(AControl);
      CreateLog(AControl);
      CreateQueue(AControl);
      CreateBottom(AControl);
    end;
  end;
end;

procedure TInfoForm.OnDestroyed(Sender: IAIMPUIForm);
begin
  tlLog := nil;
  tlQueue := nil;
  imgTree := nil;
  imgButtons := nil;
  imgLog := nil;
  btnClear := nil;
  btnRefresh := nil;
  btnFillRandom := nil;
  btnFill := nil;
  btnClearPls := nil;
  btnClearAll := nil;
  btnSettings := nil;
  FForm := nil;
end;

procedure TInfoForm.CreateHeader(AParent: IAIMPUIWinControl);
var
  AImage: IAIMPImage2;
  AControl: IAIMPUIControl;
begin
  CoreCreateObject(IAIMPImage2, AImage);
  CheckResult(AImage.LoadFromResource(HInstance, 'HEADER', 'PNG'));

  if Succeeded(FService.CreateControl(FForm, AParent, nil, Self, IAIMPUIImage, AControl)) then
  begin
    CheckResult(AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualTop, 50, NullRect)));
    CheckResult(AControl.SetValueAsObject(AIMPUI_IMAGE_PROPID_IMAGE, AImage));
  end;
end;

function TInfoForm.CreateTreeList(Parent: IAIMPUIWinControl; const Name: IAIMPString;
  Placement: TAIMPUIControlPlacement): IAIMPUITreeList;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, Self, IAIMPUITreeList, Result)) then
  begin
    CheckResult(Result.SetPlacement(Placement));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_AUTOWIDTH, 1));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_VISIBLE, 0));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_COLUMN_AUTOWIDTH, 1));
    CheckResult(Result.SetValueAsInt32(AIMPUI_TL_PROPID_BORDERS, 0));
  end;
end;

procedure TInfoForm.OnCustomDrawNode(Sender: IAIMPUITreeList; DC: HDC; R: TRect; Node: IAIMPUITreeListNode;
  var Handled: LongBool);
var
  Canvas: TCanvas;
  AString: IAIMPString;
  ARect: TRect;
begin
  if Sender = tlLog then
    Handled := True
  else
  begin
    Handled := False;
    Exit;
  end;

  Canvas := TCanvas.Create;
  try
    with Canvas do
    begin
      Handle := DC;
      if PropListGetInt32A(Node, AIMPUI_TL_NODE_PROPID_SELECTED) <> 0 then
        Brush.Color := RGB(255, 213, 159)
      else
      begin
        Node.GetValue(5, AString);
        case IAIMPStringToString(AString).ToInteger of
          0:
            Brush.Color := RGB(255, 210, 212);
          1:
            Brush.Color := RGB(240, 240, 240);
          2:
            Brush.Color := RGB(192, 233, 250);
          3:
            Brush.Color := RGB(253, 253, 203);
        end;
      end;

      // R.Height:= R.Height-100;
      FillRect(R);
      Font.Color := clBlack;
      Font.Quality := fqClearType;
      Font.Style := Font.Style + [fsBold];
      Node.GetValue(0, AString);
      ARect := Bounds(5, 5, R.Width - 10, 20);
      DrawText(Handle, IAIMPStringToString(AString), Length(IAIMPStringToString(AString)), ARect,
        DT_WORD_ELLIPSIS OR +DT_NOPREFIX);
      Font.Style := Font.Style - [fsBold];
      Node.GetValue(1, AString);
      TextOut(R.Left + 60, R.Top + 20, IAIMPStringToString(AString));
      Node.GetValue(2, AString);
      TextOut(R.Left + 60, R.Top + 35, IAIMPStringToString(AString));
      Node.GetValue(3, AString);
      TextOut(R.Left + 60, R.Top + 50, IAIMPStringToString(AString));
      Node.GetValue(4, AString);
      TextOut(R.Left + 60, R.Top + 65, IAIMPStringToString(AString));

      if PropListGetInt32A(Node, AIMPUI_WINCONTROL_PROPID_FOCUSED) = 1 then
        DrawFocusRect(R);

      Node.GetValue(5, AString);
      imgLog.Draw(DC, IAIMPStringToString(AString).ToInteger, R.Left + 15, R.Top + 30, True);
    end;
  finally
    FreeAndNil(Canvas);
  end;
end;

procedure TInfoForm.AddtoLog(const Caption, Item, Playcount, Lastplay, Reason, Style: string);
var
  ANode: IAIMPUITreeListNode;
  ARootNode: IAIMPUITreeListNode;
begin
  if Succeeded(tlLog.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
    if Succeeded(ARootNode.Add(ANode)) then
    begin
      CheckResult(ANode.SetValue(0, MakeString(Caption)));
      CheckResult(ANode.SetValue(1, MakeString(Item)));
      CheckResult(ANode.SetValue(2, MakeString(Playcount)));
      CheckResult(ANode.SetValue(3, MakeString(Lastplay)));
      CheckResult(ANode.SetValue(4, MakeString(Reason)));
      CheckResult(ANode.SetValue(5, MakeString(Style)));
      tlLog.MakeVisible(ANode);
    end;
end;

procedure TInfoForm.CreateLog(AParent: IAIMPUIWinControl);
var
  AControl: IAIMPUIWinControl;
  AColumn: IAIMPUITreeListColumn;
  ACount: Integer;
begin
  if Succeeded(FService.CreateControl(FForm, AParent, nil, nil, IAIMPUIPanel, AControl)) then
  begin
    AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualLeft, 428, NullRect));
    AParent := AControl;

    tlLog := CreateTreeList(AParent, MakeString('tlLog'), TAIMPUIControlPlacement.Create(ualClient, 0, NullRect));
    if Assigned(tlLog) then
    begin
      CheckResult(tlLog.SetValueAsInt32(AIMPUI_TL_PROPID_NODE_HEIGHT, 85));
      CheckResult(tlLog.SetValueAsInt32(AIMPUI_TL_PROPID_GRID_LINES, 2));
      CheckResult(tlLog.SetValueAsInt32(AIMPUI_TL_PROPID_CELL_HINTS, 0));
      for ACount := 0 to 5 do
        CheckResult(tlLog.AddColumn(IAIMPUITreeListColumn, AColumn));
    end;
  end;
end;

procedure TInfoForm.CreateQueue(AParent: IAIMPUIWinControl);
var
  AControl: IAIMPUIWinControl;
begin
  if Succeeded(FService.CreateControl(FForm, AParent, nil, nil, IAIMPUIPanel, AControl)) then
  begin
    AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualRight, 245, NullRect));
    AParent := AControl;

    tlQueue := CreateTreeList(AParent, MakeString('tlQueue'), TAIMPUIControlPlacement.Create(ualClient, 0, NullRect));
    if Assigned(tlQueue) then
    begin
      CheckResult(tlQueue.SetValueAsInt32(AIMPUI_TL_PROPID_NODE_HEIGHT, 24));
      CheckResult(tlQueue.SetValueAsInt32(AIMPUI_TL_PROPID_GRID_LINES, 0));
      CheckResult(tlQueue.SetValueAsObject(AIMPUI_TL_PROPID_NODE_IMAGES, imgTree));
    end;
  end;
end;

procedure TInfoForm.ShowPlaybackQueue;
var
  AKey: IAIMPPlaylist;
  ARootNode, APlaylistNode, ANode, AItemsNode: IAIMPUITreeListNode;
  APlaylistManager: IAIMPServicePlaylistManager;
  APlaylist: IAIMPPlaylist;
  AArtist: string;
begin
  try

    if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
      if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
      begin
        tlQueue.BeginUpdate;
        tlQueue.Clear;
        if Succeeded(tlQueue.GetRootNode(IID_IAIMPUITreeListNode, ARootNode)) then
          for AKey in FPlaybackQueue.Keys do
            if Succeeded(ARootNode.Add(APlaylistNode)) then
            begin
              APlaylistNode.SetValue(0, MakeString(GetPlaylistName(AKey)));
              APlaylistNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 0);
              APlaylistNode.Add(ANode);
              ANode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 1);
              ANode.SetValue(0, MakeString('Текущая позиция: ' + FPlaybackQueue[AKey].PlaybackCursor.ToString));

              APlaylistNode.Add(ANode);
              ANode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 1);
              ANode.SetValue(0, MakeString(FPlaybackQueue[AKey].SongItem.Count.ToString + '/' +
                AKey.GetItemCount.ToString));

              APlaylistNode.Add(ANode);
              ANode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 1);
              ANode.SetValue(0, MakeString('Треков не подходит: ' + FPlaybackQueue[AKey]
                .NoMathSongList.Count.ToString));

              APlaylistNode.Add(ANode);
              ANode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 1);
              ANode.SetValue(0, MakeString('Последние артисты: ' + FPlaybackQueue[AKey].LastArtist.Count.ToString));
              for AArtist in FPlaybackQueue[AKey].LastArtist do
              begin
                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                AItemsNode.SetValue(0, MakeString(AArtist));
              end;

              APlaylistNode.Add(ANode);
              ANode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 1);
              if FPlaybackQueue[AKey].OverrideSettings then
              begin
                ANode.SetValue(0, MakeString('Индивидуальные настройки: да'));
                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                AItemsNode.SetValue(0, MakeString('фильтр: ' + FPlaybackQueue[AKey].PlstFilter.ToString));

                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                AItemsNode.SetValue(0, MakeString('дней: ' + FPlaybackQueue[AKey].PlsDays.ToString));

                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                AItemsNode.SetValue(0, MakeString('вопроизведений: ' + FPlaybackQueue[AKey].PlsPlaycount.ToString));

                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                AItemsNode.SetValue(0, MakeString('артисты: ' + FPlaybackQueue[AKey].PlsArtists.ToString));

                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                if FPlaybackQueue[AKey].PlsExcept then
                  AItemsNode.SetValue(0, MakeString('исключения включены'))
                else
                  AItemsNode.SetValue(0, MakeString('исключения выключены'));

                ANode.Add(AItemsNode);
                AItemsNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_IMAGEINDEX, 2);
                if FPlaybackQueue[AKey].PlsQueueSave then
                  AItemsNode.SetValue(0, MakeString('очередь сохраняется'))
                else
                  AItemsNode.SetValue(0, MakeString('очередь не сохраняется'));
              end
              else
                ANode.SetValue(0, MakeString('Индивидуальные настройки: нет'));

              if APlaylist = AKey then
                APlaylistNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_EXPANDED, 1)
              else
                APlaylistNode.SetValueAsInt32(AIMPUI_TL_NODE_PROPID_EXPANDED, 0);
            end;
        tlQueue.EndUpdate;
      end;
  finally
    APlaylistManager := nil;
    APlaylist := nil;
  end;
end;

function TInfoForm.CreateButton(Parent: IAIMPUIWinControl; const Name: IAIMPString; ImageIndex: Integer): IAIMPUIButton;
begin
  if Succeeded(FService.CreateControl(FForm, Parent, Name, Self, IID_IAIMPUIButton, Result)) then
  begin
    CheckResult(Result.SetPlacement(TAIMPUIControlPlacement.Create(ualRight, 45)));
    CheckResult(Result.SetValueAsInt32(AIMPUI_BUTTON_PROPID_FOCUSONCLICK, 0));
    Result.SetValueAsObject(AIMPUI_BUTTON_PROPID_IMAGELIST, imgButtons);
    Result.SetValueAsInt32(AIMPUI_BUTTON_PROPID_IMAGEINDEX, ImageIndex);
  end;
end;

procedure TInfoForm.CreateBottom(AParent: IAIMPUIWinControl);
var
  AControl: IAIMPUIWinControl;
begin
  if Succeeded(FService.CreateControl(FForm, AParent, nil, nil, IAIMPUIPanel, AControl)) then
  begin
    AControl.SetPlacement(TAIMPUIControlPlacement.Create(ualBottom, 50));
    AControl.SetValueAsInt32(AIMPUI_PANEL_PROPID_TRANSPARENT, 1);
    AControl.SetValueAsInt32(AIMPUI_PANEL_PROPID_BORDERS, 0);
    AParent := AControl;

    btnClear := CreateButton(AParent, MakeString('btnClear'), 0);
    btnRefresh := CreateButton(AParent, MakeString('btnRefresh'), 4);
    btnFillRandom := CreateButton(AParent, MakeString('btnFillRandom'), 5);
    btnFill := CreateButton(AParent, MakeString('btnFill'), 3);
    btnClearPls := CreateButton(AParent, MakeString('btnClearPls'), 2);
    btnClearAll := CreateButton(AParent, MakeString('btnClearAll'), 1);
    btnSettings := CreateButton(AParent, MakeString('btnSettings'), 6);
  end;
end;

procedure TInfoForm.OnChanged(Sender: IInterface);
var
  AIntf: IAIMPUIControl;
  AControlName: string;
  AServiceOptionsDialog: IAIMPServiceOptionsDialog;
  APlaylistManager: IAIMPServicePlaylistManager;
  APlaylist: IAIMPPlaylist;
  APlaylistItem: IAIMPPlaylistItem;
  ASongList: TList<Integer>;
  ACount: Integer;
  Start: Cardinal;
begin
  if Supports(Sender, IAIMPUIControl, AIntf) then
    if PropListGetStr(AIntf, AIMPUI_CONTROL_PROPID_NAME, AControlName) then
    begin
      if AControlName = 'btnClear' then
      begin
        tlLog.Clear;
        Exit;
      end;

      if AControlName = 'btnRefresh' then
      begin
        ShowPlaybackQueue;
        Exit;
      end;
      if AControlName = 'btnFillRandom' then
      begin
        Start := GetTickCount;
        if not FShuffle then
          Exit;

        if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
        begin
          ASongList := TList<Integer>.Create;
          try
            if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
            begin
              FPlaybackQueue.CreatePlaylist(APlaylist);
              while ASongList.Count < APlaylist.GetItemCount do
              begin
                ACount := Random(APlaylist.GetItemCount);
                if not ASongList.Contains(ACount) then
                  ASongList.Add(ACount);
              end;
              for ACount := 0 to ASongList.Count - 1 do
                if Succeeded(APlaylist.GetItem(ASongList[ACount], IID_IAIMPPlaylistItem, APlaylistItem)) then
                  if not FPlaybackQueue[APlaylist].SongItem.Contains(APlaylistItem) then
                    if IsValid(APlaylist, APlaylistItem) then
                      FPlaybackQueue[APlaylist].SongItem.Add(APlaylistItem);
            end;
          finally
            DebugMessageS(GetPlaylistName(APlaylist), Format('Заполнен за %d секунд', [(GetTickCount - Start) div 1000]
              ), STYLE_NOTICE);
            APlaylistManager := nil;
            APlaylist := nil;
            FreeAndNil(ASongList);
          end;
        end;
        Exit;
      end;

      if AControlName = 'btnFill' then
      begin
        if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
        begin
          try
            if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
            begin
              FPlaybackQueue.CreatePlaylist(APlaylist);

              for ACount := 0 to APlaylist.GetItemCount - 1 do
              begin
                if Succeeded(APlaylist.GetItem(ACount, IID_IAIMPPlaylistItem, APlaylistItem)) then
                  if not FPlaybackQueue[APlaylist].SongItem.Contains(APlaylistItem) then
                    FPlaybackQueue[APlaylist].SongItem.Add(APlaylistItem);
              end;
            end;
          finally
            APlaylistManager := nil;
            APlaylist := nil;
            APlaylistItem := nil;
          end;
        end;
        Exit;
      end;

      if AControlName = 'btnClearPls' then
      begin
        if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
        begin
          try
            if Succeeded(APlaylistManager.GetActivePlaylist(APlaylist)) then
            begin
              FPlaybackQueue.CreatePlaylist(APlaylist);

              FPlaybackQueue.ClearPlaylist(APlaylist);
            end;
          finally
            APlaylistManager := nil;
            APlaylist := nil;
          end;
        end;
        Exit;
      end;

      if AControlName = 'btnClearAll' then
      begin
        FPlaybackQueue.Clear;
        Exit;
      end;

      if AControlName = 'btnSettings' then
      begin
        try
          if CoreGetService(IID_IAIMPServiceOptionsDialog, AServiceOptionsDialog) then
            CheckResult(AServiceOptionsDialog.FrameShow(FOptionFrame, True));
        finally
          AServiceOptionsDialog := nil;
        end;
      end;
    end;
end;

{$REGION 'do nothing'}

procedure TInfoForm.OnCustomDrawNodeCell(Sender: IAIMPUITreeList; DC: HDC; R: TRect; Node: IAIMPUITreeListNode;
  Column: IAIMPUITreeListColumn; var Handled: LongBool);
begin

end;

procedure TInfoForm.OnGetNodeBackground(Sender: IAIMPUITreeList; Node: IAIMPUITreeListNode; var Color: DWORD);
begin

end;

procedure TInfoForm.OnActivated(Sender: IAIMPUIForm);
begin

end;

procedure TInfoForm.OnCloseQuery(Sender: IAIMPUIForm; var CanClose: LongBool);
begin

end;

procedure TInfoForm.OnCreated(Sender: IAIMPUIForm);
begin

end;

procedure TInfoForm.OnDeactivated(Sender: IAIMPUIForm);
begin

end;

procedure TInfoForm.OnLocalize(Sender: IAIMPUIForm);
begin

end;

procedure TInfoForm.OnShortCut(Sender: IAIMPUIForm; Key, Modifiers: Word; var Handled: LongBool);
begin

end;
{$ENDREGION}

end.
