﻿unit PlaylistManager;

interface

uses Windows, apiPlaylists, System.Generics.Collections;

type
  TPlaylistListener = class(TInterfacedObject, IAIMPPlaylistListener)
  private
    FPlaylist: IAIMPPlaylist;
  protected
    procedure Activated; stdcall;
    procedure Changed(Flags: DWORD); stdcall;
    procedure Removed; stdcall;
  public
    constructor Create(const APlaylist: IAIMPPlaylist);
    destructor Destroy; override;
  end;

  TPlaylistManager = class(TInterfacedObject, IAIMPExtensionPlaylistManagerListener)
  private
    FListenerList: TDictionary<IAIMPPlaylist, TPlaylistListener>;
  protected
    procedure PlaylistActivated(Playlist: IAIMPPlaylist); stdcall;
    procedure PlaylistAdded(Playlist: IAIMPPlaylist); stdcall;
    procedure PlaylistRemoved(Playlist: IAIMPPlaylist); stdcall;
  public
    constructor Create;
    destructor Destroy; override;
  end;

implementation

uses
  ActiveX, defines, System.Sysutils, apiWrappers;

{$REGION 'TPlaylistManager'}

constructor TPlaylistManager.Create();
begin
  FListenerList := TDictionary<IAIMPPlaylist, TPlaylistListener>.Create;
  CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServicePlaylistManager, Self));
end;

destructor TPlaylistManager.Destroy;
var
  AKey: IAIMPPlaylist;
begin
  for AKey in FListenerList.Keys do
  begin
    FListenerList[AKey].Removed;
    FListenerList[AKey] := nil;
  end;
  FreeAndNil(FListenerList);

  inherited;
end;

procedure TPlaylistManager.PlaylistActivated(Playlist: IAIMPPlaylist);
begin
  // do nothing
end;

procedure TPlaylistManager.PlaylistAdded(Playlist: IAIMPPlaylist);
begin
  if not FListenerList.ContainsKey(Playlist) then
    FListenerList.Add(Playlist, TPlaylistListener.Create(Playlist));

  if FEnable and FShuffle then
  begin
    FPlaybackQueue.CreatePlaylist(Playlist);
  end;
end;

procedure TPlaylistManager.PlaylistRemoved(Playlist: IAIMPPlaylist);
begin
  if FListenerList.ContainsKey(Playlist) then
  begin
    FListenerList[Playlist].Removed;
    FListenerList[Playlist] := nil;
    FListenerList.Remove(Playlist);
  end;

  if FPlaybackQueue.ContainsKey(Playlist) then
  begin
    FPlaybackQueue.Remove(Playlist);
    FPlaybackQueue.TrimExcess;

    if FCurrentPlaylist = Playlist then
      FCurrentPlaylist := nil;
  end;
end;
{$ENDREGION}
{$REGION 'TPlaylistListener'}

constructor TPlaylistListener.Create(const APlaylist: IAIMPPlaylist);
begin
  FPlaylist := APlaylist;
  CheckResult(FPlaylist.ListenerAdd(Self));
end;

destructor TPlaylistListener.Destroy;
begin
  FPlaylist := nil;
  inherited;
end;

procedure TPlaylistListener.Activated;
begin
  //
end;

procedure TPlaylistListener.Changed(Flags: DWORD);
begin
  if Flags and AIMP_PLAYLIST_NOTIFY_CONTENT <> 0 then
  begin
    if FShuffle then
      FPlaybackQueue.RemoveDeletedItems(FPlaylist);
  end;
end;

procedure TPlaylistListener.Removed;
begin
  CheckResult(FPlaylist.ListenerRemove(Self));
end;
{$ENDREGION}

end.
