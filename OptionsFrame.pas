﻿unit OptionsFrame;

interface

uses Winapi.Windows, OptionsForm, apiOptions, apiObjects;

type
  TAIMPPluginOptionFrame = class(TInterfacedObject, IAIMPOptionsDialogFrame)
  strict private
    FFrame: TOptionsFrame;
    procedure HandlerModified(Sender: TObject);
  protected
    function CreateFrame(ParentWnd: HWND): HWND; stdcall;
    procedure DestroyFrame; stdcall;
    function GetName(out S: IAIMPString): HRESULT; stdcall;
    procedure Notification(ID: Integer); stdcall;
  public
    property OptionFrame: TOptionsFrame read FFrame;
  end;

implementation

uses ActiveX, apiGUI, System.Sysutils, apiWrappers;

function TAIMPPluginOptionFrame.CreateFrame(ParentWnd: HWND): HWND;
var
  R: Trect;
  AService: IAIMPServiceUI;
begin
  if CoreGetService(IAIMPServiceUI, AService) then
  begin
    FFrame := TOptionsFrame.Create(ParentWnd, AService);
    FFrame.OnModified := HandlerModified;
    GetWindowRect(ParentWnd, R);
    OffsetRect(R, -R.Left, -R.Top);
    FFrame.Form.SetPlacement(TAIMPUIControlPlacement.Create(R));
    Result := FFrame.Form.GetHandle;
  end;
end;

procedure TAIMPPluginOptionFrame.DestroyFrame;
begin
  if Assigned(FFrame.Form) then
  begin
    FFrame.Form.Release(False);
    FFrame.Form := nil;
  end;

  if Assigned(FFrame) then
    FFrame := nil;
end;

function TAIMPPluginOptionFrame.GetName(out S: IAIMPString): HRESULT;
begin
  try
    S := MakeString('SkipTrack');
    Result := S_OK;
  except
    Result := E_UNEXPECTED;
  end;
end;

procedure TAIMPPluginOptionFrame.Notification(ID: Integer);
begin
  if FFrame <> nil then
    case ID of
      AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_LOCALIZATION:
        TOptionsFrame(FFrame).ApplyLocalization;
      AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_LOAD:
        TOptionsFrame(FFrame).ConfigLoad;
      AIMP_SERVICE_OPTIONSDIALOG_NOTIFICATION_SAVE:
        TOptionsFrame(FFrame).ConfigSave;
    end;
end;

procedure TAIMPPluginOptionFrame.HandlerModified(Sender: TObject);
var
  AServiceOptions: IAIMPServiceOptionsDialog;
begin
  if Supports(CoreIntf, IAIMPServiceOptionsDialog, AServiceOptions) then
    AServiceOptions.FrameModified(Self);
end;

end.
