﻿unit Main;

interface

uses
  PlaylistManager, OptionsForm, Windows, AIMPCustomPlugin, apiPlugin, apiCore,
  PlaybackExtension, apiMessages, System.Generics.Collections;

type
  TAIMPTrackSkip = class(TAIMPCustomPlugin, IAIMPMessageHook)
  private
    FPlaylistManager: TPlaylistManager;
    FExtPlaybackQueue: TAIMPExtPlaybackQueue;
    procedure LoadSettings;
    procedure SaveSettings;
  protected
    function InfoGet(Index: Integer): PWideChar; override; stdcall;
    function InfoGetCategories: Cardinal; override; stdcall;
    function Initialize(Core: IAIMPCore): HRESULT; override; stdcall;
    procedure Finalize; override; stdcall;
    procedure CoreMessage(Message: DWORD; Param1: Integer; Param2: Pointer; var Result: HRESULT); stdcall;
  end;

implementation

uses
  Defines, PlaybackQueue, OptionsFrame, Menu, apiPlaylists, apiGUI,
  WideStrings, apiOptions, apiMUI, ActiveX, System.Sysutils, apiFileManager,
  apiWrappers, {$IFDEF DEBUG}InfoForm, {$ENDIF}apiPlayer;

{$REGION 'IAIMPMessageHook'}

procedure TAIMPTrackSkip.CoreMessage(Message: DWORD; Param1: Integer; Param2: Pointer; var Result: HRESULT);
var
  AKey: IAIMPPlaylist;
begin
  case Message of
    AIMP_MSG_EVENT_PROPERTY_VALUE:
      begin
        if Param1 = AIMP_MSG_PROPERTY_SHUFFLE then
        begin
          FShuffle := PLongBool(Param2)^;
          if not FShuffle then
          begin
            for AKey in FPlaybackQueue.Keys do
              FPlaybackQueue.ClearPlaylist(AKey);

            FCurrentPlaylist := nil;
          end;
        end;
      end;
    AIMP_MSG_EVENT_LOADED:
      begin
        FPlaybackQueue.LoadFromXML(CoreGetProfilePath + '\queue.xml');
        FExtPlaybackQueue := TAIMPExtPlaybackQueue.Create;
        CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServicePlaybackQueue, FExtPlaybackQueue));
        {$IFDEF DEBUG}CheckResult(FInfoForm.Form.SetValueAsInt32(AIMPUI_CONTROL_PROPID_VISIBLE, 1)); {$ENDIF}
      end;
    AIMP_MSG_EVENT_STREAM_END:
      begin
        if FShuffle then
        begin
          if Param1 and AIMP_MES_END_OF_PLAYLIST <> 0 then
            if (FCurrentPlaylist <> nil) and FPlaybackQueue.ContainsKey(FCurrentPlaylist) then
            begin
              if FPlaybackQueue[FCurrentPlaylist].PlaylistEnd then
              begin
                {$IFDEF DEBUG}
                if Assigned(FInfoForm.Form) then
                  FInfoForm.DebugMessageS(GetPlaylistName(FCurrentPlaylist), 'Плейлист закончен, очищаем очередь',
                    STYLE_NOTICE);
                {$ENDIF}
                FPlaybackQueue.ClearPlaylist(FCurrentPlaylist);

                FCurrentPlaylist := nil;
                {$IFDEF DEBUG} FCurrentSong := nil; {$ENDIF}
              end;
            end;
        end;
      end;
  end;
end;
{$ENDREGION}
{$REGION 'TAIMPCustomPlugin'}

function TAIMPTrackSkip.InfoGet(Index: Integer): PWideChar;
begin
  case Index of
    AIMP_PLUGIN_INFO_NAME:
      Result := 'SkipTrack 2.0.4';
    AIMP_PLUGIN_INFO_AUTHOR:
      Result := 'Awakunar';
    AIMP_PLUGIN_INFO_SHORT_DESCRIPTION:
      Result := 'Skip tracks in shuffle mode from playback queue';
  else
    Result := nil;
  end;
end;

function TAIMPTrackSkip.Initialize(Core: IAIMPCore): HRESULT;
var
  ALoaded: LongBool;
  APlaylistManager: IAIMPServicePlaylistManager;
  ADispatcher: IAIMPServiceMessageDispatcher;
  AUIService: IAIMPServiceUI;
  MenuService: TAIMPMenuService;
begin
  Randomize;

  Result := Core.QueryInterface(IID_IAIMPServiceUI, AUIService);
  if Succeeded(Result) then
  begin
    Result := Core.QueryInterface(IID_IAIMPServicePlaylistManager, APlaylistManager);
    if Succeeded(Result) then
    begin
      Result := inherited Initialize(Core);
      if Succeeded(Result) then
      begin
        try
          FExceptions := TWideStringList.Create;
          FPlaybackQueue := TPlaybackQueue.Create([doOwnsValues]);
          LoadSettings;
          FPlaylistManager := TPlaylistManager.Create;
          {$IFDEF DEBUG}FInfoForm := TInfoForm.Create(AUIService); {$ENDIF}
          if CoreGetService(IAIMPServiceMessageDispatcher, ADispatcher) then
          begin
            CheckResult(ADispatcher.Hook(Self));
            // обработка загрузки плагина после инициализации программы
            if Succeeded(ADispatcher.Send(AIMP_MSG_PROPERTY_LOADED, AIMP_MSG_PROPVALUE_GET, @ALoaded)) then
              if ALoaded then
              begin
                FPlaybackQueue.LoadFromXML(CoreGetProfilePath + '\queue.xml');
                CheckResult(ADispatcher.Send(AIMP_MSG_PROPERTY_SHUFFLE, AIMP_MSG_PROPVALUE_GET, @FShuffle));

                FExtPlaybackQueue := TAIMPExtPlaybackQueue.Create;
                CheckResult(CoreIntf.RegisterExtension(IID_IAIMPServicePlaybackQueue, FExtPlaybackQueue));
                {$IFDEF DEBUG}CheckResult(FInfoForm.Form.SetValueAsInt32(AIMPUI_CONTROL_PROPID_VISIBLE, 1)); {$ENDIF}
              end;
          end;
          MenuService := TAIMPMenuService.Create;

          FOptionFrame := TAIMPPluginOptionFrame.Create;
          CheckResult(Core.RegisterExtension(IID_IAIMPServiceOptionsDialog, FOptionFrame));
        finally
          ADispatcher := nil;
          APlaylistManager := nil;
          AUIService := nil;
          FreeAndNil(MenuService);
        end;
      end;
    end;
  end;
end;

procedure TAIMPTrackSkip.LoadSettings;
var
  AConfig: TAIMPServiceConfig;
begin
  AConfig := TAIMPServiceConfig.Create;
  try
    with AConfig do
    begin
      FEnable := ReadBool('aimp_skip\enable', true);
      FPlaybackQueue.ExceptEnable := ReadBool('aimp_skip\except_enable', true);
      FPlaybackQueue.QueueSave := ReadBool('aimp_skip\queue_save', False);
      FPlaybackQueue.ExceptFav := ReadBool('aimp_skip\except_5stars', False);
      FPlaybackQueue.Filter := ReadInteger('aimp_skip\filter', 1);
      FPlaybackQueue.Days := ReadInteger('aimp_skip\skipdays', 3);
      FPlaybackQueue.Artists := ReadInteger('aimp_skip\artists', 5);
      FPlaybackQueue.Playcount := ReadInteger('aimp_skip\playcount', 5);
    end;
  finally
    FreeAndNil(AConfig);
  end;
  if FileExists(CoreGetProfilePath + '\exceptions.txt') then
    FExceptions.LoadFromFile(CoreGetProfilePath + '\exceptions.txt', TEncoding.UTF8);
end;

procedure TAIMPTrackSkip.SaveSettings;
var
  AConfig: TAIMPServiceConfig;
begin
  AConfig := TAIMPServiceConfig.Create;
  try
    with AConfig do
    begin
      WriteBool('aimp_skip\enable', FEnable);
      WriteBool('aimp_skip\except_enable', FPlaybackQueue.ExceptEnable);
      WriteBool('aimp_skip\queue_save', FPlaybackQueue.QueueSave);
      WriteBool('aimp_skip\except_5stars', FPlaybackQueue.ExceptFav);
      WriteInteger('aimp_skip\filter', FPlaybackQueue.Filter);
      WriteInteger('aimp_skip\skipdays', FPlaybackQueue.Days);
      WriteInteger('aimp_skip\artists', FPlaybackQueue.Artists);
      WriteInteger('aimp_skip\playcount', FPlaybackQueue.Playcount);
    end;
  finally
    FreeAndNil(AConfig);
  end;

  FPlaybackQueue.SaveToXML(CoreGetProfilePath + '\queue.xml');
end;

function TAIMPTrackSkip.InfoGetCategories: Cardinal;
begin
  Result := AIMP_PLUGIN_CATEGORY_ADDONS;
end;

procedure TAIMPTrackSkip.Finalize;
var
  ADispatcher: IAIMPServiceMessageDispatcher;
begin
  SaveSettings;
  if CoreGetService(IAIMPServiceMessageDispatcher, ADispatcher) then
    ADispatcher.Unhook(Self);
  ADispatcher := nil;
  FCurrentPlaylist := nil;
  FPlaylistManager := nil;
  FExtPlaybackQueue := nil;
  FOptionFrame := nil;
  FreeAndNil(FExceptions);
  FreeAndNil(FPlaybackQueue);
  {$IFDEF DEBUG}
  FCurrentSong := nil;
  FInfoForm := nil;
  {$ENDIF}
end;
{$ENDREGION}

end.
