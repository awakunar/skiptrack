﻿unit CheckSong;

interface

uses apiPlaylists;

function IsValid(const Playlist: IAIMPPlaylist; PlaylistItem: IAIMPPlaylistItem): Boolean;

implementation

uses Defines, ActiveX, System.Sysutils, System.DateUtils,
  apiObjects, apiWrappers, apiFileManager;

var
  FPlaylist: IAIMPPlaylist;
  FFileInfo: IAIMPFileInfo;
  FPlaylistItem: IAIMPPlaylistItem;

function HasFiveStars: Boolean;
var
  AStars: double;
begin
  Result := False;
  AStars := 0;

  if Succeeded(FFileInfo.GetValueAsFloat(AIMP_FILEINFO_PROPID_STAT_MARK, AStars)) then
    Result := AStars = 5;
end;

function ArtistInExceptions: Boolean;
var
  ACount: Integer;
  AString: IAIMPString;
begin
  Result := False;

  if PropListGetStr(FFileInfo, AIMP_FILEINFO_PROPID_ARTIST, AString) then
  begin
    for ACount := 0 to FExceptions.Count - 1 do
      if WideSameText(IAIMPStringToString(AString), FExceptions[ACount]) then
      begin
        Result := True;
        Break;
      end;
  end;
end;

function IsInExceptions(const ExceptFav: Boolean): Boolean;
begin
  Result := False;

  if ExceptFav and HasFiveStars then
  begin
    {$IFDEF DEBUG}
    if Assigned(FInfoForm.Form) then
      FInfoForm.DebugMessageG(FPlaylistItem, 'Трек в исключениях: Оценка 5 звезд', STYLE_NOTICE);
    {$ENDIF}
    Exit(True);
  end;

  if ArtistInExceptions then
  begin
    {$IFDEF DEBUG}
    if Assigned(FInfoForm.Form) then
      FInfoForm.DebugMessageG(FPlaylistItem, 'Исполнитель в исключениях', STYLE_NOTICE);
    {$ENDIF}
    Result := True;
  end;
end;

function DateMatch(const Days: Integer): Boolean;
var
  ADate: double;
  ASongDate: TDateTime;
begin
  ADate := 0;

  FFileInfo.GetValueAsFloat(AIMP_FILEINFO_PROPID_STAT_LASTPLAYDATE, ADate);
  ASongDate := ADate;
  Result := DaysBetween(Date, ASongDate) > Days;
end;

function PlaycountMatch(const Playcount: Integer): Boolean;
var
  APlayCount: Integer;
begin
  APlayCount := 0;

  FFileInfo.GetValueAsInt32(AIMP_FILEINFO_PROPID_STAT_PLAYCOUNT, APlayCount);
  Result := APlayCount <= Playcount;
end;

function NotLastArtist(const Artists: Integer): Boolean;
var
  AArtist, AKey: string;
begin
  Result := True;

  if Artists = 0 then
    Exit;

  AArtist := PropListGetStr(FFileInfo, AIMP_FILEINFO_PROPID_ARTIST);
  for AKey in FPlaybackQueue[FPlaylist].LastArtist do
    if WideSameText(AArtist, AKey) then
    begin
      Result := False;
      Break;
    end;

  if Result then
    FPlaybackQueue[FPlaylist].LastArtist.Enqueue(AArtist);

  while FPlaybackQueue[FPlaylist].LastArtist.Count > Artists do
    FPlaybackQueue[FPlaylist].LastArtist.Dequeue;
end;

function SongMatch(const Filter, Days, Playcount, Artists: Integer): Boolean;
var
  NoMathResult: Boolean;
begin
  Result := False;
  NoMathResult := True;

  case Filter of
    0:
      Result := True;
    1:
      begin
        NoMathResult := DateMatch(Days);
        Result := NoMathResult;
      end;
    2:
      begin
        NoMathResult := PlaycountMatch(Playcount);
        Result := NoMathResult;
      end;
    3:
      begin
        NoMathResult := DateMatch(Days) and PlaycountMatch(Playcount);
        Result := NoMathResult;
      end;
    4:
      Result := NotLastArtist(Artists);
    5:
      begin
        NoMathResult := DateMatch(Days);
        Result := NoMathResult and NotLastArtist(Artists);
      end;
    6:
      begin
        NoMathResult := PlaycountMatch(Playcount);
        Result := NoMathResult and NotLastArtist(Artists);
      end;
    7:
      begin
        NoMathResult := DateMatch(Days) and PlaycountMatch(Playcount);
        Result := NoMathResult and NotLastArtist(Artists);
      end;
  end;

  if not NoMathResult then
    FPlaybackQueue[FPlaylist].NoMathSongList.Add(FPlaylistItem);

  {$IFDEF DEBUG}
  if Result and Assigned(FInfoForm.Form) then
    FInfoForm.DebugMessageG(FPlaylistItem, '', STYLE_SUCCESS);
  {$ENDIF}
end;

function IsValid(const Playlist: IAIMPPlaylist; PlaylistItem: IAIMPPlaylistItem): Boolean;
begin
  Result := False;
  FPlaylistItem := PlaylistItem;
  FPlaylist := Playlist;
  try
    if Succeeded(PlaylistItem.GetValueAsObject(AIMP_PLAYLISTITEM_PROPID_FILEINFO, IID_IAIMPFileInfo, FFileInfo)) then
      if FPlaybackQueue[Playlist].OverrideSettings then
      begin
        if FPlaybackQueue[Playlist].PlsExcept then
          if IsInExceptions(FPlaybackQueue[Playlist].PlsExceptFav) then
            Exit(True);

        Result := SongMatch(FPlaybackQueue[Playlist].PlstFilter, FPlaybackQueue[Playlist].PlsDays,
          FPlaybackQueue[Playlist].PlsPlaycount, FPlaybackQueue[Playlist].PlsArtists);
      end
      else
      begin
        if FPlaybackQueue.ExceptEnable then
          if IsInExceptions(FPlaybackQueue.ExceptFav) then
            Exit(True);

        Result := SongMatch(FPlaybackQueue.Filter, FPlaybackQueue.Days, FPlaybackQueue.Playcount,
          FPlaybackQueue.Artists);
      end;
  finally
    FFileInfo := nil;
    FPlaylistItem := nil;
    FPlaylist := nil;
  end;
end;

end.
