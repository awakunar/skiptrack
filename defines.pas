﻿unit Defines;

interface

uses
  {$IFDEF DEBUG}InfoForm, {$ENDIF} PlaybackQueue, OptionsFrame, apiPlaylists,
  apiObjects, System.WideStrings;

{$IFDEF DEBUG}

const
  STYLE_ERROR: string = '0';
  STYLE_SUCCESS: string = '1';
  STYLE_NOTICE: string = '2';
  STYLE_WARNING: string = '3';
  {$ENDIF}
function GetPlaylistByID(const ID: string): IAIMPPlaylist;
function GetPlaylistID(const Playlist: IAIMPPlaylist): string;
function GetPlaylistName(const Playlist: IAIMPPlaylist): string;

function PropListGetInt32A(const List: IAIMPPropertyList; ID: Integer): Integer; overload;
{$IFDEF INLINE_SUPPORTED}inline; {$ENDIF}
function PropListGetInt32A(const List: IAIMPPropertyList; ID: Integer; out S: Integer): LongBool; overload;
{$IFDEF INLINE_SUPPORTED}inline; {$ENDIF}
function PropListGetBool(const List: IAIMPPropertyList; ID: Integer): Boolean; overload;
{$IFDEF INLINE_SUPPORTED}inline; {$ENDIF}
function PropListGetBool(const List: IAIMPPropertyList; ID: Integer; out S: Boolean): Boolean; overload;
{$IFDEF INLINE_SUPPORTED}inline;
{$ENDIF}

var
  {$IFDEF DEBUG}
  FInfoForm: TInfoForm;
  FCurrentSong: IAIMPPlaylistItem;
  {$ENDIF}
  FOptionFrame: TAIMPPluginOptionFrame;
  FShuffle: LongBool;
  FEnable: Boolean;
  FExceptions: TWideStringList;
  FPlaybackQueue: TPlaybackQueue;
  FCurrentPlaylist: IAIMPPlaylist;

implementation

uses ActiveX, System.Sysutils, apiWrappers;

function GetPlaylistByID(const ID: string): IAIMPPlaylist;
var
  APlaylistManager: IAIMPServicePlaylistManager;
begin
  Result := nil;

  if CoreGetService(IID_IAIMPServicePlaylistManager, APlaylistManager) then
    APlaylistManager.GetLoadedPlaylistByID(MakeString(ID), Result);
end;

function GetPlaylistName(const Playlist: IAIMPPlaylist): string;
var
  APropertyList: IAIMPPropertyList;
begin
  try
    if Succeeded(Playlist.QueryInterface(IID_IAIMPPropertyList, APropertyList)) then
      Result := PropListGetStr(APropertyList, AIMP_PLAYLIST_PROPID_NAME);
  finally
    APropertyList := nil;
  end;
end;

function GetPlaylistID(const Playlist: IAIMPPlaylist): string;
var
  APropertyList: IAIMPPropertyList;
begin
  try
    if Succeeded(Playlist.QueryInterface(IID_IAIMPPropertyList, APropertyList)) then
      Result := PropListGetStr(APropertyList, AIMP_PLAYLIST_PROPID_ID);
  finally
    APropertyList := nil;
  end;
end;

function PropListGetInt32A(const List: IAIMPPropertyList; ID: Integer): Integer;
begin
  if not PropListGetInt32A(List, ID, Result) then
    Result := -1;
end;

function PropListGetInt32A(const List: IAIMPPropertyList; ID: Integer; out S: Integer): LongBool;
begin
  Result := Succeeded(List.GetValueAsInt32(ID, S));
end;

function PropListGetBool(const List: IAIMPPropertyList; ID: Integer): Boolean;
begin
  if not PropListGetBool(List, ID, Result) then
    Result := False;
end;

function PropListGetBool(const List: IAIMPPropertyList; ID: Integer; out S: Boolean): Boolean;
var
  AResult: Integer;
begin
  Result := PropListGetInt32A(List, ID, AResult);
  if Result then
    S := AResult.ToBoolean;
end;

end.
